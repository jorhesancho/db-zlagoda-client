import "./App.css";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import LoginComponent from "./auth/components/login.component";
import ModalProvider from "./modals/modal-provider";
import { ModalContainer } from "./modals/modal-container";
import { AuthGuard } from "./auth/guards/auth.guard";
import { AuthProvider } from "./auth/context/auth-provider";
import { UnauthGuard } from "./auth/guards/unauth.guard";
import { ProductsPage } from "./product/products-page";
import { CategoriesPage } from "./category/categories-page";
import { EmployeesPage } from "./employee/employees-page";
import { StoreProductsPage } from "./store-product/store-products-page";
import { StoreProductSearchPage } from "./store-product/store-product-search/store-product-search-page";
import { CustomerCardsPage } from "./customer-card/customer-card-page";
import { EmployeeSearchPage } from "./employee/employee-search/employee-search-page";
import { ProfilePage } from "./employee/profile/profile-page";
import { ChecksPage } from "./check/checks-page";
import { NotificationContextProvider } from "./notification/context/NotificationContexProvider";
import { SalePage } from "./sale/sale-page";
import { SaleStatsPage } from "./sales/sales-stats.page";
import { CheckSearchPage } from "./check/check-search/check-search-page";
import { RoleGuard } from "./auth/guards/role.guard";
import { Role } from "./employee/employee.model";
import { Dashboard } from "./sales/dashboard-page";

function App() {
  return (
    <div className="App">
      <ModalProvider>
        <NotificationContextProvider>
          <AuthProvider>
            <Router>
              <Routes>
                <Route
                  index
                  element={
                    <AuthGuard>
                      <Dashboard />
                    </AuthGuard>
                  }
                ></Route>
                <Route
                  path="/login"
                  element={
                    <UnauthGuard>
                      <LoginComponent />
                    </UnauthGuard>
                  }
                  index
                />
                <Route
                  path="/categories"
                  element={
                    <AuthGuard>
                      <CategoriesPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/products"
                  element={
                    <AuthGuard>
                      <ProductsPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/store-products"
                  element={
                    <AuthGuard>
                      <StoreProductsPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/store-products-search"
                  element={
                    <AuthGuard>
                      <StoreProductSearchPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/customer-cards"
                  element={
                    <AuthGuard>
                      <CustomerCardsPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/employees"
                  element={
                    <AuthGuard>
                      <EmployeesPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/employees-search"
                  element={
                    <AuthGuard>
                      <EmployeeSearchPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/checks"
                  element={
                    <AuthGuard>
                      <ChecksPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/check-search"
                  element={
                    <AuthGuard>
                      <CheckSearchPage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/profile"
                  element={
                    <AuthGuard>
                      <ProfilePage />
                    </AuthGuard>
                  }
                />
                <Route
                  path="/sale"
                  element={
                    <RoleGuard roles={[Role.Cashier]}>
                      <SalePage />
                    </RoleGuard>
                  }
                />
                <Route
                  path="/sale-stats"
                  element={
                    <AuthGuard>
                      <SaleStatsPage />
                    </AuthGuard>
                  }
                />
              </Routes>
            </Router>
            <ModalContainer />
          </AuthProvider>
        </NotificationContextProvider>
      </ModalProvider>
    </div>
  );
}

export default App;
