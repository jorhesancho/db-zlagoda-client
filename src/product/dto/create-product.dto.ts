export class CreateProductDto {
  name: string;
  characteristics: string;
  categoryNumber: number;
}
