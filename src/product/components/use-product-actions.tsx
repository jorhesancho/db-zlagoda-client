import { Modal } from "../../modals/modal";
import { useModal } from "../../modals/use-modal";
import { CreateProductDto } from "../dto/create-product.dto";
import { Product } from "../product.model";
import { productService } from "../product.service";
import CreateProductForm from "./create-product-form";
import DeleteProductForm from "./delete-product-form";

export const useProductActions = (params: { reload?: () => any }) => {
  const { openModal } = useModal();
  const { reload = () => {} } = params;

  const submitCreateProduct = (data: CreateProductDto) => {
    productService.create(data).then(() => {
      reload();
    });
  };

  const submitUpdateProduct =
    (productId: number) => (data: CreateProductDto) => {
      productService.update(productId, data).then(() => {
        reload();
      });
    };

  const submitDeleteProduct = (productId: number) => () => {
    productService.delete(productId).then(() => {
      reload();
    });
  };

  const openCreateModal = () => {
    openModal(
      <Modal title="Create product">
        <CreateProductForm onSubmit={submitCreateProduct} />
      </Modal>
    );
  };

  const openEditModal = (product: Product) => {
    openModal(
      <Modal title="Edit product">
        <CreateProductForm
          onSubmit={submitUpdateProduct(product.id)}
          defaultProduct={product}
        />
      </Modal>
    );
  };

  const openDeleteModal = (product: Product) => {
    openModal(
      <Modal title="Delete product">
        <DeleteProductForm
          product={product}
          onDelete={submitDeleteProduct(product.id)}
        />
      </Modal>
    );
  };

  return {
    openCreateModal,
    openEditModal,
    openDeleteModal,
  };
};
