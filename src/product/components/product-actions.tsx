import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { Product } from "../product.model";
import { useProductActions } from "./use-product-actions";
import { Role } from "../../employee/employee.model";

interface BookActionsProps {
  product: Product;
  reload?: () => any;
}

export const ProductActions: FC<BookActionsProps> = ({ product, reload }) => {
  const actions = useProductActions({
    reload,
  });

  return (
    <ForRoles roles={[Role.Manager]}>
      <div className="d-flex flex-row gap-2">
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => actions.openEditModal(product)}
        >
          Edit
        </button>

        <button
          type="button"
          className="btn btn-danger"
          onClick={() => actions.openDeleteModal(product)}
        >
          Delete
        </button>
      </div>
    </ForRoles>
  );
};
