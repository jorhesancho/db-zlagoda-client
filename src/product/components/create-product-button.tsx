import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { useProductActions } from "./use-product-actions";
import { Role } from "../../employee/employee.model";

interface Props {
  reload: () => any;
}

export const CreateProductButton: FC<Props> = ({ reload }) => {
  const { openCreateModal } = useProductActions({ reload });

  return (
    <ForRoles roles={[Role.Manager]}>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => openCreateModal()}
      >
        Create Product
      </button>
    </ForRoles>
  );
};
