import React, { useEffect, useState } from "react";
import { categoryService } from "../../category/category.service";
import { Category } from "../../category/category.model";

interface FilterProps {
  onSelect: (category: Category | undefined) => any;
}

const ProductFilters: React.FC<FilterProps> = ({ onSelect }) => {
  const [categories, setCategories] = useState<Category[]>([]);
  const [selectedCategory, setSelectedCategory] = useState<
    Category | undefined
  >(undefined);

  const loadCategories = () => {
    categoryService.getAll().then((res) => setCategories(res));
  };

  useEffect(() => {
    loadCategories();
  }, []);

  const handleCategoryChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const categoryNumber = e.target.value;

    const category = categories.find(
      (category) => `${category.number}` === categoryNumber
    );
    setSelectedCategory(category);
    onSelect(category);
  };

  return (
    <div className="container">
      <div className="row align-items-center">
        <div className="col-auto">
          <label htmlFor="categorySelect" className="form-label mb-0">
            Category
          </label>
        </div>

        <div className="col-auto">
          <select
            className="form-select"
            id="categorySelect"
            value={selectedCategory?.number || ""}
            onChange={handleCategoryChange}
          >
            <option value="">All</option>
            {categories.map((category) => (
              <option key={category.number} value={category.number}>
                {category.name}
              </option>
            ))}
          </select>
        </div>
      </div>
    </div>
  );
};

export default ProductFilters;
