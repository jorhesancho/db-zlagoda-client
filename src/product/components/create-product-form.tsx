import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useModal } from "../../modals/use-modal";
import { pickFrom } from "../../shared/utils/pick";
import { nullable } from "pratica";
import { CreateProductDto } from "../dto/create-product.dto";
import { createYupSchema, fieldConfig } from "../../shared/utils/field-config";
import { categoryService } from "../../category/category.service";
import { Category } from "../../category/category.model";

const fields = [
  fieldConfig({
    name: "name",
    label: "Name",
    type: "text",
    validation: yup.string().required("Name is required"),
  }),
  fieldConfig({
    name: "characteristics",
    label: "Characteristics",
    type: "textarea",
    validation: yup.string().required("Characteristics are required"),
  }),
  fieldConfig({
    name: "categoryNumber",
    label: "Category number",
    type: "number",
    validation: yup
      .number()
      .typeError("Invalid category")
      .required("Category Number is required"),
  }),
] as const;

const schema = createYupSchema(fields);

interface CreateProductFormProps {
  onSubmit: (data: CreateProductDto) => any;
  defaultProduct?: CreateProductDto;
}

const mapToProductDto = (product: CreateProductDto): CreateProductDto => {
  return pickFrom(product, ["name", "characteristics", "categoryNumber"]);
};

const CreateProductForm: React.FC<CreateProductFormProps> = ({
  onSubmit,
  defaultProduct,
}) => {
  const { closeModal } = useModal();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CreateProductDto>({
    defaultValues: nullable(defaultProduct).map(mapToProductDto).value(),
    resolver: yupResolver(schema),
  });

  const myOnSubmit = (data: CreateProductDto) => {
    closeModal();
    onSubmit(data);
  };

  const [categories, setCategories] = useState<Category[]>([]);

  const loadCategories = () => {
    categoryService.getAll().then((res) => setCategories(res));
  };

  useEffect(() => {
    loadCategories();
  }, []);

  const simpleFields = fields.filter((f) => f.name !== "categoryNumber");

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form
            onSubmit={handleSubmit(myOnSubmit)}
            className="d-flex flex-column justify-content-start gap-3"
          >
            {simpleFields.map((field) => (
              <div
                className="form-group d-flex flex-column align-items-start gap-1"
                key={field.name}
              >
                <label className="fw-bold" htmlFor={field.name}>
                  {field.label}
                </label>
                {field.type === "textarea" ? (
                  <textarea
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                ) : (
                  <input
                    type={field.type}
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                )}
                {errors[field.name] && (
                  <div className="invalid-feedback">
                    {errors[field.name]?.message}
                  </div>
                )}
              </div>
            ))}

            <div className="form-group d-flex flex-column align-items-start gap-1">
              <label className="fw-bold" htmlFor="categoryNumber">
                Category
              </label>

              <select
                className={`form-control ${
                  errors.categoryNumber ? "is-invalid" : ""
                }`}
                id="categoryNumber"
                {...register("categoryNumber")}
              >
                <option value="">Select a category</option>
                {categories.map((category) => (
                  <option
                    key={category.number}
                    value={category.number}
                    selected={
                      category.number === defaultProduct?.categoryNumber
                    }
                  >
                    {category.name}
                  </option>
                ))}
              </select>
              {errors.categoryNumber && (
                <div className="invalid-feedback">
                  {errors.categoryNumber?.message}
                </div>
              )}
            </div>

            <button type="submit" className="btn btn-primary mt-3">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateProductForm;
