import { FC, useCallback, useEffect, useRef, useState } from "react";
import { ProductRow } from "./product-row";
import { Product } from "../product.model";
import { productService } from "../product.service";
import { CreateProductButton } from "./create-product-button";
import ProductFilters from "./product-filters";
import { Category } from "../../category/category.model";
import { PrintButton } from "../../shared/components/print-btn";

interface ProductListProps {}

export const ProductList: FC<ProductListProps> = () => {
  const [products, setProducts] = useState<Product[]>([]);
  const [selectedCategory, setSelectedCategory] = useState<
    Category | undefined
  >(undefined);

  const loadProducts = useCallback(() => {
    productService.getAll(selectedCategory?.number).then(setProducts);
  }, [selectedCategory]);

  useEffect(() => {
    loadProducts();
  }, [loadProducts]);

  const reload = () => loadProducts();

  const componentRef = useRef(null);

  return (
    <div className="container mt-5">
      <div className="d-flex gap-2 align-items-center mb-4">
        <PrintButton componentRef={componentRef} />
      </div>

      <ProductFilters onSelect={setSelectedCategory} />

      <table className="table table-striped" ref={componentRef}>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Product Name</th>
            <th scope="col">Characteristics</th>
            <th scope="col">Category</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product) => (
            <ProductRow key={product.id} product={product} reload={reload} />
          ))}
        </tbody>
      </table>

      <CreateProductButton reload={reload} />
    </div>
  );
};
