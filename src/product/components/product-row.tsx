import { FC } from "react";
import { Product } from "../product.model";
import { ProductActions } from "./product-actions";

interface ProductRowProps {
  product: Product;
  reload?: () => any;
}

export const ProductRow: FC<ProductRowProps> = ({ product, reload }) => {
  return (
    <tr className="table-row-link">
      <th scope="row">{product.id}</th>
      <td>{product.name}</td>
      <td>{product.characteristics}</td>
      <td>{product.category?.name}</td>
      <td>
        <div className="d-flex flex-row justify-content-end">
          <div className="d-flex flex-row gap-2">
            <ProductActions product={product} reload={reload} />
          </div>
        </div>
      </td>
    </tr>
  );
};
