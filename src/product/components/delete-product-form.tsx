import React, { FC } from "react";
import { useModal } from "../../modals/use-modal";
import { Product } from "../product.model";

interface DeleteProductFormProps {
  onDelete: () => void;
  product: Product;
}

const DeleteProductForm: FC<DeleteProductFormProps> = ({
  onDelete,
  product,
}) => {
  const { closeModal } = useModal();

  const myOnDelete = () => {
    closeModal();
    onDelete();
  };

  return (
    <div className="d-flex flex-column gap-3 align-items-start text-start">
      <p className="fs-5 m-0">
        Are you sure you want to delete the product "{product.name}"?
      </p>

      <div className="d-flex flex-row align-items-start gap-3">
        <button type="button" className="btn btn-danger" onClick={myOnDelete}>
          Delete
        </button>

        <button
          type="button"
          className="btn btn-secondary"
          onClick={closeModal}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

export default DeleteProductForm;
