import { Category } from "../category/category.model";

export class Product {
  id: number;
  categoryNumber: number;
  name: string;
  characteristics: string;
  category?: Category;
}
