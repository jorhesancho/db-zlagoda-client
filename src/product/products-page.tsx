import { FC } from "react";
import Layout from "../shared/components/layout";
import { ProductList } from "./components/product-list";
import { PageTitle } from "../shared/components/page-title";

export const ProductsPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Products" />
      <ProductList />
    </Layout>
  );
};
