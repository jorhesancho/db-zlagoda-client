import { environment } from "../environment/environment";
import { HttpClient } from "../shared/http/http-client";
import { CreateProductDto } from "./dto/create-product.dto";
import { Product } from "./product.model";

export class ProductService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(dto: CreateProductDto): Promise<void> {
    await this.httpClient.post("/", dto);
  }

  public getAll(categoryNumber?: number): Promise<Product[]> {
    const params = categoryNumber ? { categoryNumber } : {};

    return this.httpClient.get<Product[]>("/", { params });
  }

  public getOneById(id: number): Promise<Product> {
    return this.httpClient.get<Product[]>(`/${id}`).then((res) => res[0]);
  }

  public async update(id: number, dto: CreateProductDto): Promise<void> {
    await this.httpClient.patch(`/${id}`, dto);
  }

  public async delete(id: number): Promise<void> {
    await this.httpClient.delete(`/${id}`);
  }
}

export const productService = new ProductService(
  new HttpClient(`${environment.apiUrl}/products`)
);
