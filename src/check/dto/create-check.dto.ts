import { CreateSaleDto } from "./create-sale.dto";

export class CreateCheckDto {
  checkNumber: string;
  employeeId: string;
  cardNumber?: string;
  printDate: string;
  sumTotal: number;
  valueAddedTax: number;
  sales: CreateSaleDto[];
}
