export class CreateSaleDto {
  upc: string;
  sellingPrice: number;
  amount: number;
}
