import * as yup from "yup";
import { fieldConfig } from "../../shared/utils/field-config";

export const createCheckFields = [
  fieldConfig({
    name: "checkNumber",
    label: "Check number",
    type: "text",
    validation: yup.string().max(10).required("Check number is required"),
  }),
  fieldConfig({
    name: "cardNumber",
    label: "Card number",
    type: "text",
    validation: yup.string(),
  }),
] as const;
