import { FC, Fragment } from "react";
import { CheckActions } from "./check-actions";
import { formatDate } from "../../shared/utils/format-date";
import { SalesList } from "./sales-list";
import { Check } from "../models/check.model";

interface CheckRowProps {
  check: Check;
  reload?: () => any;
  expanded: string | null;
  handleExpand: (checkNumber: string) => any;
}

export const CheckRow: FC<CheckRowProps> = ({
  check,
  reload,
  expanded,
  handleExpand,
}) => {
  const currency = "UAH";

  return (
    <Fragment key={check.checkNumber}>
      <tr
        className={`bg-light table-row-link cursor-pointer accordion-toggle ${
          expanded === check.checkNumber ? "expanded" : ""
        }`}
        style={{ cursor: "pointer" }}
        onClick={() => handleExpand(check.checkNumber)}
      >
        <th scope="row">{check.checkNumber}</th>
        <td>{check.employeeId}</td>
        <td>{check.cardNumber ?? "-"}</td>
        <td>{formatDate(check.printDate, "YYYY.MM.DD HH:mm:ss")}</td>
        <td>{check.sumTotal.toFixed(2)}</td>
        <td>{check.valueAddedTax.toFixed(2)}</td>
        <td>
          <CheckActions check={check} reload={reload} />
        </td>
      </tr>
      {expanded === check.checkNumber && !!check.sales?.length && (
        <tr>
          <td colSpan={6}>
            <SalesList sales={check.sales} currency={currency} />
          </td>
        </tr>
      )}
    </Fragment>
  );
};
