import { FC } from "react";
import { Sale } from "../models/sale.model";

interface Props {
  sales: Sale[];
  currency?: string;
}

export const SalesList: FC<Props> = ({ sales, currency = "UAH" }) => {
  return (
    <ul className="list-group">
      {sales.map((sale) => (
        <li className="list-group-item" key={`${sale.upc}-${sale.checkNumber}`}>
          <strong>{sale.storeProduct?.product?.name}</strong>: {sale.amount}x,{" "}
          {sale.sellingPrice} {currency}
        </li>
      ))}
    </ul>
  );
};
