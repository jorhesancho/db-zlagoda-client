import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { Role } from "../../employee/employee.model";
import { Link } from "react-router-dom";

interface Props {
  reload: () => any;
}

export const CreateCheckButton: FC<Props> = ({ reload }) => {
  return (
    <ForRoles roles={[Role.Cashier]}>
      <Link to={"/sale"} className="btn btn-primary">
        Create Check
      </Link>
    </ForRoles>
  );
};
