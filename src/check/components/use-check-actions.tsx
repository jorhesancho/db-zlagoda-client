import { Modal } from "../../modals/modal";
import { useModal } from "../../modals/use-modal";
import { CreateCheckDto } from "../dto/create-check.dto";
import { checkService } from "../check.service";
import CreateCheckForm from "../../sale/create-check-form";
import DeleteCheckForm from "./delete-check-form";
import { Check } from "../models/check.model";

export const useCheckActions = (params: { reload?: () => any }) => {
  const { openModal } = useModal();
  const { reload = () => {} } = params;

  const submitCreateCheck = (data: CreateCheckDto) => {
    checkService.create(data).then(() => {
      reload();
    });
  };

  const submitDeleteCheck = (checkId: string) => () => {
    checkService.delete(checkId).then(() => {
      reload();
    });
  };

  const openCreateModal = () => {
    openModal(
      <Modal title="Create check">
        <CreateCheckForm onSubmit={submitCreateCheck} />
      </Modal>
    );
  };

  const openDeleteModal = (check: Check) => {
    openModal(
      <Modal title="Delete check">
        <DeleteCheckForm
          check={check}
          onDelete={submitDeleteCheck(check.checkNumber)}
        />
      </Modal>
    );
  };

  return {
    openCreateModal,
    openDeleteModal,
  };
};
