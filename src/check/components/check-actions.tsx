import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { useCheckActions } from "./use-check-actions";
import { Role } from "../../employee/employee.model";
import { Check } from "../models/check.model";

interface CheckActionsProps {
  check: Check;
  reload?: () => any;
}

export const CheckActions: FC<CheckActionsProps> = ({ check, reload }) => {
  const actions = useCheckActions({
    reload,
  });

  return (
    <ForRoles roles={[Role.Manager]}>
      <div className="d-flex flex-row gap-2">
        <button
          type="button"
          className="btn btn-danger"
          onClick={(e) => {
            e.stopPropagation();
            actions.openDeleteModal(check);
          }}
        >
          Delete
        </button>
      </div>
    </ForRoles>
  );
};
