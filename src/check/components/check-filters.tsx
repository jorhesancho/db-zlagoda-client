import { FC, useEffect, useState } from "react";
import { Employee, Role } from "../../employee/employee.model";
import { employeeService } from "../../employee/employee.service";
import CashierSelect from "../../sales/components/cashier-select";
import { DateFilters } from "../../sales/components/date-filters";
import { DateQuery } from "../../sales/dto/date.dto";
import { Check } from "../models/check.model";
import { checkService } from "../check.service";

interface Props {
  setChecks: (v: Check[]) => any;
}

export const CheckFilters: FC<Props> = ({ setChecks }) => {
  const [dateQuery, setDateQuery] = useState<DateQuery>({
    startDate: "",
    endDate: "",
  });
  const [employee, setEmployee] = useState<Employee | undefined>();
  const [employees, setEmployees] = useState<Employee[]>([]);

  useEffect(() => {
    employeeService.getAll(Role.Cashier).then(setEmployees);
  }, []);

  const isValidQuery = (dates: DateQuery) => {
    return !!dates.startDate && !!dates.endDate;
  };

  useEffect(() => {
    if (!isValidQuery(dateQuery)) {
      return;
    }
    checkService.findAllByDates(dateQuery, employee?.id).then(setChecks);
  }, [dateQuery, employee, setChecks]);

  return (
    <div className="mb-4">
      <DateFilters onDateChange={setDateQuery} />
      <CashierSelect onValueChange={setEmployee} employees={employees} />
    </div>
  );
};
