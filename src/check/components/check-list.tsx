import React, { useEffect, useRef, useState } from "react";
import { checkService } from "../check.service";
import { CreateCheckButton } from "./create-check-button";
import { CheckRow } from "./check-row";
import { Check } from "../models/check.model";
import { PrintButton } from "../../shared/components/print-btn";
import SearchCheckButton from "../check-search/search-check-button";
import { CheckFilters } from "./check-filters";

interface Props {}

const CheckList: React.FC<Props> = () => {
  const [expanded, setExpanded] = useState<string | null>(null);
  const [checks, setChecks] = useState<Check[]>([]);

  const loadChecks = () => {
    checkService.getAll().then(setChecks);
  };

  useEffect(() => {
    loadChecks();
  }, []);

  const reload = () => loadChecks();

  const handleExpand = (checkNumber: string) => {
    setExpanded(expanded === checkNumber ? null : checkNumber);
  };

  const componentRef = useRef(null);

  return (
    <div className="container">
      <div className="d-flex gap-2 align-items-center mb-4">
        <SearchCheckButton />

        <PrintButton componentRef={componentRef} />
      </div>

      <CheckFilters setChecks={setChecks} />

      <table className="table" ref={componentRef}>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Employee id</th>
            <th scope="col">Card</th>
            <th scope="col">Print date</th>
            <th scope="col">Sum total</th>
            <th scope="col">VAT</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>

        <tbody>
          {checks.map((check) => (
            <CheckRow
              key={check.checkNumber}
              check={check}
              reload={reload}
              expanded={expanded}
              handleExpand={handleExpand}
            />
          ))}
        </tbody>
      </table>

      <CreateCheckButton reload={reload} />
    </div>
  );
};

export default CheckList;
