import { environment } from "../environment/environment";
import { DateQuery, formatDateQuery } from "../sales/dto/date.dto";
import { HttpClient } from "../shared/http/http-client";
import { CreateCheckDto } from "./dto/create-check.dto";
import { Check } from "./models/check.model";

export class CheckService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(dto: CreateCheckDto): Promise<void> {
    await this.httpClient.post("/", dto);
  }

  public getAll(): Promise<Check[]> {
    return this.httpClient.get<Check[]>("/");
  }

  public findAllByDates(
    dateQuery: DateQuery,
    employeeId?: string
  ): Promise<Check[]> {
    if (employeeId) {
      return this.findAllByDatesAndCashier(employeeId, dateQuery);
    }

    return this.httpClient.get<Check[]>("/by-dates", {
      params: formatDateQuery(dateQuery),
    });
  }

  public findAllByDatesAndCashier(
    employeeId: string,
    dateQuery: DateQuery
  ): Promise<Check[]> {
    return this.httpClient.get<Check[]>(`/by-dates/${employeeId}`, {
      params: formatDateQuery(dateQuery),
    });
  }

  public getOneById(id: string): Promise<Check> {
    return this.httpClient.get<Check>(`/${id}`);
  }

  public async delete(id: string): Promise<void> {
    await this.httpClient.delete(`/${id}`);
  }
}

export const checkService = new CheckService(
  new HttpClient(`${environment.apiUrl}/checks`)
);
