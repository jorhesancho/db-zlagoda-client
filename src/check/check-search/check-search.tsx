import { useState } from "react";
import { CheckCard } from "./check-card";
import { Check } from "../models/check.model";
import { checkService } from "../check.service";

const CheckSearch = () => {
  const [surname, setSurname] = useState("");
  const [employee, setCheck] = useState<Check | undefined>(undefined);

  const searchCheck = () => {
    if (surname) {
      checkService.getOneById(surname).then((res) => {
        setCheck(res);
      });
    }
  };

  return (
    <div className="container">
      <div className="row mt-5">
        <div className="col-md-6 offset-md-3">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search check by id"
              value={surname}
              onChange={(e) => setSurname(e.target.value)}
            />
            <button
              className="btn btn-outline-primary"
              type="button"
              onClick={searchCheck}
            >
              Search
            </button>
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={() => {
                setSurname("");
                setCheck(undefined);
              }}
            >
              Clear
            </button>
          </div>
        </div>
      </div>
      {employee && <CheckCard check={employee} />}
    </div>
  );
};

export default CheckSearch;
