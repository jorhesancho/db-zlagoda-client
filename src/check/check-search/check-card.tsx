import { FC } from "react";
import { Check } from "../models/check.model";
import { formatDate } from "../../shared/utils/format-date";
import { SalesList } from "../components/sales-list";

export const CheckCard: FC<{ check: Check }> = ({ check }) => {
  return (
    <div className="row">
      <div className="col-md-6 offset-md-3">
        <div className="card shadow">
          <div className="card-body">
            <h5 className="card-title">
              <strong>Check {check.checkNumber}</strong>
            </h5>

            <div className="dropdown-divider"></div>

            <div className="d-flex flex-column gap-2">
              <p className="card-text m-0">
                <strong>Card:</strong> {check.cardNumber}
              </p>
              <p className="card-text m-0">
                <strong>Employee ID:</strong> {check.employeeId}
              </p>
              <p className="card-text m-0">
                <strong>Print date:</strong>{" "}
                {formatDate(check.printDate, "YYYY.MM.DD HH:mm:ss")}
              </p>
              <p className="card-text m-0">
                <strong>Sum total:</strong> {check.sumTotal}
              </p>
              <p className="card-text m-0">
                <strong>Value added tax:</strong> {check.valueAddedTax}
              </p>

              <p className="card-text m-0">
                <strong>Items:</strong>
              </p>

              {check?.sales && <SalesList sales={check.sales} />}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
