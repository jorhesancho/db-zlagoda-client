import { FC } from "react";
import Layout from "../../shared/components/layout";
import CheckSearch from "./check-search";
import { PageTitle } from "../../shared/components/page-title";

export const CheckSearchPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Check Search" />
      <CheckSearch />
    </Layout>
  );
};
