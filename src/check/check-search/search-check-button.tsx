import { Link } from "react-router-dom";

const SearchCheckButton = () => {
  return (
    <Link to="/check-search" className="btn btn-primary" role="button">
      Search
    </Link>
  );
};

export default SearchCheckButton;
