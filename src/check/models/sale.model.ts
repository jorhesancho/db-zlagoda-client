import { StoreProduct } from "../../store-product/store-product.model";

export class Sale {
  upc: string;
  checkNumber: string;
  sellingPrice: number;
  amount: number;
  storeProduct?: StoreProduct;
}
