import { Sale } from "./sale.model";

export class Check {
  checkNumber: string;
  employeeId: string;
  cardNumber?: string;
  printDate: string;
  sumTotal: number;
  valueAddedTax: number;
  sales?: Sale[];
}
