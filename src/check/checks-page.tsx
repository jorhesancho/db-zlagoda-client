import { FC } from "react";
import Layout from "../shared/components/layout";
import CheckList from "./components/check-list";
import { PageTitle } from "../shared/components/page-title";

export const ChecksPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Checks" />
      <CheckList />
    </Layout>
  );
};
