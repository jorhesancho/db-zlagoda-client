import { useState } from "react";

export const usePagination = (defaultSize = 10) => {
  const [page, setPage] = useState(1);
  const [size, setSize] = useState(defaultSize);

  const handlePreviousPage = () => {
    if (page > 1) {
      setPage(page - 1);
    }
  };

  const handleNextPage = () => {
    setPage(page + 1);
  };

  return { size, page, handleNextPage, handlePreviousPage };
};
