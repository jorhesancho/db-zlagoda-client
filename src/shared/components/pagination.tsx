import { FC } from "react";

interface PaginationProps {
  hasNext: boolean;
  handlePreviousPage: () => any;
  handleNextPage: () => any;
  page: number;
}

export const Pagination: FC<PaginationProps> = ({
  page,
  hasNext,
  handleNextPage,
  handlePreviousPage,
}) => {
  return (
    <div className="d-flex flex-row gap-3 align-items-center">
      <button
        className="btn btn-outline-secondary"
        onClick={handlePreviousPage}
        disabled={page === 1}
      >
        Previous
      </button>
      <span>Page {page}</span>
      <button
        className="btn btn-outline-secondary"
        onClick={handleNextPage}
        disabled={!hasNext}
      >
        Next
      </button>
    </div>
  );
};
