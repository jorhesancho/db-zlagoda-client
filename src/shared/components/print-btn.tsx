import { FC, MutableRefObject } from "react";
import ReactToPrint from "react-to-print";

export const PrintButton: FC<{ componentRef: MutableRefObject<null> }> = ({
  componentRef,
}) => {
  return (
    <ReactToPrint
      trigger={() => <button className="btn btn-primary">Print</button>}
      bodyClass=".print-body"
      content={() => componentRef.current}
    />
  );
};
