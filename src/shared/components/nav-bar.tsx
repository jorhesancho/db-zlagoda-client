import React from "react";
import { Link } from "react-router-dom";
import { useAuthContext } from "../../auth/context/auth-context";
import { ProfileButton } from "../../employee/profile/profile-button";
import { loggedInRoutes, loggedOutRoutes } from "../routes";

const Navbar: React.FC = () => {
  const { currentUser, logout } = useAuthContext();

  const routes = currentUser
    ? loggedInRoutes.filter(
        (route) => !route.roles || route.roles.includes(currentUser.role)
      )
    : loggedOutRoutes;

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <div className="container">
        <Link className="navbar-brand" to="/">
          Zlahoda Store App
        </Link>

        <div className="navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            {routes.map((route) => (
              <li className="nav-item" key={route.title}>
                <Link className="nav-link" to={route.path}>
                  {route.title}
                </Link>
              </li>
            ))}
          </ul>

          {currentUser && (
            <div className="d-flex flex-row gap-3 align-items-center">
              <ProfileButton currentUser={currentUser} />

              <button className="btn btn-secondary" onClick={logout}>
                Logout
              </button>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
};

export default Navbar;
