import React, { FC, PropsWithChildren } from "react";
import { NotificationBar } from "../../notification/components/NotificationBar";
import Navbar from "./nav-bar";

interface LayoutProps {
  center?: boolean;
}

const Layout: FC<PropsWithChildren<LayoutProps>> = ({
  children,
  center = true,
}) => {
  const alignmentStyle = center
    ? "d-flex justify-content-center"
    : "d-flex justify-content-start";

  return (
    <>
      <header>
        <Navbar />
      </header>
      <NotificationBar />
      <main className={`container mt-4 ${alignmentStyle}`}>
        <div className="row" style={{ width: "100%" }}>
          <div className="col">
            <div className="">{children}</div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Layout;
