import { Role } from "../employee/employee.model";

export interface Route {
  title: string;
  path: string;
  roles?: Role[];
}

export const loggedInRoutes: Route[] = [
  {
    title: "Categories",
    path: "/categories",
  },
  {
    title: "Products",
    path: "/products",
  },
  {
    title: "Employees",
    path: "/employees",
  },
  {
    title: "Store Products",
    path: "/store-products",
  },
  {
    title: "Customer cards",
    path: "/customer-cards",
  },
  {
    title: "Checks",
    path: "/checks",
  },
  {
    title: "Sale",
    path: "/sale",
    roles: [Role.Cashier],
  },
  {
    title: "Stats",
    path: "/sale-stats",
  },
];

export const loggedOutRoutes: Route[] = [
  {
    title: "Login",
    path: "/login",
  },
];
