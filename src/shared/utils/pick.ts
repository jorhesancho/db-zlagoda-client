export const pick = <T, K extends keyof T>(...keys: K[]) => {
  return (item: T): Pick<T, K> => {
    return keys.reduce((record, key) => {
      record[key] = item[key];

      return record;
    }, {} as Pick<T, K>);
  };
};

export const pickFrom = <T, K extends keyof T>(item: T, keys: K[]) => {
  return pick<T, K>(...keys)(item);
};
