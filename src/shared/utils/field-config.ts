import { Schema } from "yup";
import * as yup from "yup";
import { InputType } from "./input-type";

export interface FieldConfig<Name extends string> {
  name: Name;
  type: InputType;
  label: string;
  validation?: Schema;
}

export const fieldConfig = <Name extends string, T extends FieldConfig<Name>>(
  config: T
): T => {
  return config;
};

export const createYupSchema = (fields: ReadonlyArray<FieldConfig<any>>) => {
  const shape = fields.reduce<Record<string, Schema>>((acc, field) => {
    if (field.validation) {
      acc[field.name] = field.validation;
    }

    return acc;
  }, {});

  return yup.object().shape(shape);
};
