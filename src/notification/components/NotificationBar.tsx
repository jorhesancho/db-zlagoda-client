import { useNotification } from "../context/NotificationContexProvider";
import { Notification } from "./Notification";

export const NotificationBar = () => {
  const { notification } = useNotification();

  return notification != null ? <Notification {...notification} /> : null;
};
