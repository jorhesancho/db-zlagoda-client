import { Product } from "../product/product.model";

export class StoreProduct {
  upc: string;
  upcProm: string;
  productId: number;
  sellingPrice: number;
  numberOfProducts: number;
  isPromotional: boolean;
  product?: Product;
}
