import { FC } from "react";
import { StoreProduct } from "../store-product.model";

export const StoreProductCard: FC<{ storeProduct: StoreProduct }> = ({
  storeProduct,
}) => {
  return (
    <div className="row">
      <div className="col-md-6 offset-md-3">
        <div className="card shadow">
          <div className="card-body">
            <h5 className="card-title">
              <strong>UPC {storeProduct.upc}</strong>
            </h5>

            <div className="dropdown-divider"></div>

            <div className="d-flex flex-column gap-2">
              <p className="card-text m-0">
                <strong>Selling price:</strong> {storeProduct.sellingPrice}
              </p>
              <p className="card-text m-0">
                <strong>Number of products:</strong>{" "}
                {storeProduct.numberOfProducts}
              </p>
              <p className="card-text m-0">
                <strong>Product name:</strong> {storeProduct.product?.name}
              </p>
              <p className="card-text m-0">
                <strong>Product characteristics:</strong>{" "}
                {storeProduct.product?.characteristics}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
