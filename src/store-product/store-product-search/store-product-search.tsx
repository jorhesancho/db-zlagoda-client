import { useState } from "react";
import { StoreProduct } from "../store-product.model";
import { storeProductService } from "../store-product.service";
import { StoreProductCard } from "./store-product-card";

const StoreProductSearch = () => {
  const [upc, setUpc] = useState("");
  const [storeProduct, setStoreProduct] = useState<StoreProduct | undefined>(
    undefined
  );

  const searchProduct = () => {
    if (upc) {
      storeProductService.getOneById(upc).then((res) => {
        setStoreProduct(res);
      });
    }
  };

  return (
    <div className="container">
      <div className="row mt-5">
        <div className="col-md-6 offset-md-3">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search by UPC"
              value={upc}
              onChange={(e) => setUpc(e.target.value)}
            />
            <button
              className="btn btn-outline-primary"
              type="button"
              onClick={searchProduct}
            >
              Search
            </button>
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={() => {
                setUpc("");
                setStoreProduct(undefined);
              }}
            >
              Clear
            </button>
          </div>
        </div>
      </div>
      {storeProduct && <StoreProductCard storeProduct={storeProduct} />}
    </div>
  );
};

export default StoreProductSearch;
