import { Link } from "react-router-dom";

const SearchStoreProductButton = () => {
  return (
    <Link to="/store-products-search" className="btn btn-primary" role="button">
      Search
    </Link>
  );
};

export default SearchStoreProductButton;
