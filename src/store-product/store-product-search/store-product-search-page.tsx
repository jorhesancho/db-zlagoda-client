import { FC } from "react";
import Layout from "../../shared/components/layout";
import StoreProductSearch from "./store-product-search";
import { PageTitle } from "../../shared/components/page-title";

export const StoreProductSearchPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Store Product Search" />
      <StoreProductSearch />
    </Layout>
  );
};
