import { FC } from "react";
import Layout from "../shared/components/layout";
import { StoreProductList } from "./components/store-product-list";
import { PageTitle } from "../shared/components/page-title";

export const StoreProductsPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Store Products" />
      <StoreProductList />
    </Layout>
  );
};
