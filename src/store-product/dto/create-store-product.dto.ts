export class CreateStoreProductDto {
  upc: string;
  upcProm?: string;
  productId: number;
  sellingPrice: number;
  numberOfProducts: number;
  isPromotional: boolean;
}
