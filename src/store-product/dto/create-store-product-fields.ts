import * as yup from "yup";
import { createYupSchema, fieldConfig } from "../../shared/utils/field-config";

export const createStoreProductFields = [
  fieldConfig({
    name: "upc",
    label: "UPC",
    type: "text",
    validation: yup.string().required("UPC is required"),
  }),
  fieldConfig({
    name: "upcProm",
    label: "UPC Prom",
    type: "text",
    validation: yup.string(),
  }),
  fieldConfig({
    name: "productId",
    label: "Product ID",
    type: "number",
    validation: yup
      .number()
      .typeError("Invalid product")
      .required("Product ID is required"),
  }),
  fieldConfig({
    name: "sellingPrice",
    label: "Selling Price",
    type: "number",
    validation: yup
      .number()
      .typeError("Invalid price")
      .required("Selling Price is required"),
  }),
  fieldConfig({
    name: "numberOfProducts",
    label: "Number of Products",
    type: "number",
    validation: yup
      .number()
      .typeError("Invalid number of products")
      .required("Number of Products is required"),
  }),
  fieldConfig({
    name: "isPromotional",
    label: "Is Promotional",
    type: "checkbox",
    validation: yup.boolean(),
  }),
] as const;

export const createStoreProductSchema = createYupSchema(
  createStoreProductFields
);
