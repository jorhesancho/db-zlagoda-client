import { environment } from "../environment/environment";
import { HttpClient } from "../shared/http/http-client";
import { CreateStoreProductDto } from "./dto/create-store-product.dto";
import { StoreProduct } from "./store-product.model";

export class StoreProductService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(dto: CreateStoreProductDto): Promise<void> {
    await this.httpClient.post("/", dto);
  }

  public getAll(): Promise<StoreProduct[]> {
    return this.httpClient.get<StoreProduct[]>("/");
  }

  public getOneById(upc: string): Promise<StoreProduct> {
    return this.httpClient.get<StoreProduct>(`/${upc}`);
  }

  public async update(upc: string, dto: CreateStoreProductDto): Promise<void> {
    await this.httpClient.patch(`/${upc}`, dto);
  }

  public async delete(upc: string): Promise<void> {
    await this.httpClient.delete(`/${upc}`);
  }
}

export const storeProductService = new StoreProductService(
  new HttpClient(`${environment.apiUrl}/store-products`)
);
