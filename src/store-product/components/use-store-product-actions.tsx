import { Modal } from "../../modals/modal";
import { useModal } from "../../modals/use-modal";
import { CreateStoreProductDto } from "../dto/create-store-product.dto";
import { StoreProduct } from "../store-product.model";
import { storeProductService } from "../store-product.service";
import CreateStoreProductForm from "./create-store-product-form";
import DeleteStoreProductForm from "./delete-store-product-form";

export const useStoreProductActions = (params: { reload?: () => any }) => {
  const { openModal } = useModal();
  const { reload = () => {} } = params;

  const submitCreateStoreProduct = (data: CreateStoreProductDto) => {
    storeProductService.create(data).then(() => {
      reload();
    });
  };

  const submitUpdateStoreProduct =
    (upc: string) => (data: CreateStoreProductDto) => {
      storeProductService.update(upc, data).then(() => {
        reload();
      });
    };

  const submitDeleteStoreProduct = (upc: string) => () => {
    storeProductService.delete(upc).then(() => {
      reload();
    });
  };

  const openCreateModal = () => {
    openModal(
      <Modal title="Create storeProduct">
        <CreateStoreProductForm onSubmit={submitCreateStoreProduct} />
      </Modal>
    );
  };

  const openEditModal = (storeProduct: StoreProduct) => {
    openModal(
      <Modal title="Edit storeProduct">
        <CreateStoreProductForm
          onSubmit={submitUpdateStoreProduct(storeProduct.upc)}
          defaultStoreProduct={storeProduct}
        />
      </Modal>
    );
  };

  const openDeleteModal = (storeProduct: StoreProduct) => {
    openModal(
      <Modal title="Delete storeProduct">
        <DeleteStoreProductForm
          storeProduct={storeProduct}
          onDelete={submitDeleteStoreProduct(storeProduct.upc)}
        />
      </Modal>
    );
  };

  return {
    openCreateModal,
    openEditModal,
    openDeleteModal,
  };
};
