import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { StoreProduct } from "../store-product.model";
import { useStoreProductActions } from "./use-store-product-actions";
import { Role } from "../../employee/employee.model";

interface BookActionsProps {
  storeProduct: StoreProduct;
  reload?: () => any;
}

export const StoreProductActions: FC<BookActionsProps> = ({
  storeProduct,
  reload,
}) => {
  const actions = useStoreProductActions({
    reload,
  });

  return (
    <ForRoles roles={[Role.Manager]}>
      <div className="d-flex flex-row gap-2">
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => actions.openEditModal(storeProduct)}
        >
          Edit
        </button>

        <button
          type="button"
          className="btn btn-danger"
          onClick={() => actions.openDeleteModal(storeProduct)}
        >
          Delete
        </button>
      </div>
    </ForRoles>
  );
};
