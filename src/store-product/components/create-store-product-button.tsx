import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { useStoreProductActions } from "./use-store-product-actions";
import { Role } from "../../employee/employee.model";

interface Props {
  reload: () => any;
}

export const CreateStoreProductButton: FC<Props> = ({ reload }) => {
  const { openCreateModal } = useStoreProductActions({ reload });

  return (
    <ForRoles roles={[Role.Manager]}>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => openCreateModal()}
      >
        Create Store Product
      </button>
    </ForRoles>
  );
};
