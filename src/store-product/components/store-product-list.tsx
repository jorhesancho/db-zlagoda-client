import { FC, useEffect, useRef, useState } from "react";
import { StoreProductRow } from "./store-product-row";
import { StoreProduct } from "../store-product.model";
import { storeProductService } from "../store-product.service";
import { CreateStoreProductButton } from "./create-store-product-button";
import SearchStoreProductButton from "../store-product-search/search-products-button";
import { PrintButton } from "../../shared/components/print-btn";

interface StoreProductListProps {}

export const StoreProductList: FC<StoreProductListProps> = () => {
  const [storeProducts, setStoreProducts] = useState<StoreProduct[]>([]);

  const loadStoreProducts = () => {
    storeProductService.getAll().then(setStoreProducts);
  };

  useEffect(() => {
    loadStoreProducts();
  }, []);

  const reload = () => loadStoreProducts();

  const componentRef = useRef(null);

  return (
    <div className="container mt-5">
      <div className="d-flex gap-2 align-items-center mb-4">
        <SearchStoreProductButton />

        <PrintButton componentRef={componentRef} />
      </div>

      <table className="table table-striped" ref={componentRef}>
        <thead>
          <tr>
            <th scope="col">UPC</th>
            <th scope="col">UPC prom</th>
            <th scope="col">Product</th>
            <th scope="col">Selling price</th>
            <th scope="col">Amount</th>
            <th scope="col">Is promotional</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {storeProducts.map((storeProduct) => (
            <StoreProductRow
              key={storeProduct.upc}
              storeProduct={storeProduct}
              reload={reload}
            />
          ))}
        </tbody>
      </table>

      <CreateStoreProductButton reload={reload} />
    </div>
  );
};
