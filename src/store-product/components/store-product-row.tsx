import { FC } from "react";
import { StoreProduct } from "../store-product.model";
import { StoreProductActions } from "./store-product-actions";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheck, faTimes } from "@fortawesome/free-solid-svg-icons";

interface StoreProductRowProps {
  storeProduct: StoreProduct;
  reload?: () => any;
}

export const StoreProductRow: FC<StoreProductRowProps> = ({
  storeProduct,
  reload,
}) => {
  return (
    <tr className="table-row-link">
      <th scope="row">{storeProduct.upc}</th>
      <td>{storeProduct.upcProm}</td>
      <td>{storeProduct.product?.name || storeProduct.productId}</td>
      <td>{storeProduct.sellingPrice}</td>
      <td>{storeProduct.numberOfProducts}</td>
      <td>
        {
          <span
            className="d-flex align-items-center justify-content-center"
            style={{ width: "30px", height: "30px" }}
          >
            {storeProduct.isPromotional ? (
              <FontAwesomeIcon icon={faCheck} size="lg" />
            ) : (
              <FontAwesomeIcon icon={faTimes} size="lg" />
            )}
          </span>
        }
      </td>

      <td>
        <div className="d-flex flex-row justify-content-end">
          <div className="d-flex flex-row gap-2">
            <StoreProductActions storeProduct={storeProduct} reload={reload} />
          </div>
        </div>
      </td>
    </tr>
  );
};
