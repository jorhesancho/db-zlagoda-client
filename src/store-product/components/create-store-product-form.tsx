import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useModal } from "../../modals/use-modal";
import { pickFrom } from "../../shared/utils/pick";
import { nullable } from "pratica";
import { CreateStoreProductDto } from "../dto/create-store-product.dto";
import {
  createStoreProductFields,
  createStoreProductSchema,
} from "../dto/create-store-product-fields";

interface CreateStoreProductFormProps {
  onSubmit: (data: CreateStoreProductDto) => any;
  defaultStoreProduct?: CreateStoreProductDto;
}

const mapToStoreProductDto = (
  storeProduct: CreateStoreProductDto
): CreateStoreProductDto => {
  return pickFrom(storeProduct, [
    "upc",
    "upcProm",
    "productId",
    "sellingPrice",
    "numberOfProducts",
    "isPromotional",
  ]);
};

const CreateStoreProductForm: React.FC<CreateStoreProductFormProps> = ({
  onSubmit,
  defaultStoreProduct,
}) => {
  const { closeModal } = useModal();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CreateStoreProductDto>({
    defaultValues: nullable(defaultStoreProduct)
      .map(mapToStoreProductDto)
      .value(),
    resolver: yupResolver(createStoreProductSchema),
  });

  const myOnSubmit = (data: CreateStoreProductDto) => {
    closeModal();
    onSubmit({ ...data, upcProm: data.upcProm || undefined });
  };

  const fields = createStoreProductFields;

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form
            onSubmit={handleSubmit(myOnSubmit)}
            className="d-flex flex-column justify-content-start gap-3"
          >
            {fields.map((field) => (
              <div
                className="form-group d-flex flex-column align-items-start gap-1"
                key={field.name}
              >
                <label className="fw-bold" htmlFor={field.name}>
                  {field.label}
                </label>
                {field.type === "checkbox" ? (
                  <input
                    type={field.type}
                    className={`form-check-input ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                ) : (
                  <input
                    type={field.type}
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                )}
                {errors[field.name] && (
                  <div className="invalid-feedback">
                    {errors[field.name]?.message}
                  </div>
                )}
              </div>
            ))}

            <button type="submit" className="btn btn-primary mt-3">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateStoreProductForm;
