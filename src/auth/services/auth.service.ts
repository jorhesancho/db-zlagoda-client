import { Employee } from "../../employee/employee.model";
import { environment } from "../../environment/environment";
import { HttpClient, httpClient } from "../../shared/http/http-client";
import { LoginDto } from "../dto/login-dto";
import { LoginResponse } from "../dto/login-response.dto";
import { SignupDto } from "../dto/signup.dto";
import { TokenService, tokenService } from "./token.service";
import jwtDecode from "jwt-decode";

export class AuthService {
  constructor(private http: HttpClient, private tokenService: TokenService) {}

  public async login(data: LoginDto): Promise<string> {
    const res = await this.http.post<LoginResponse>(
      `${environment.apiUrl}/auth/sign-in`,
      data
    );

    const { accessToken } = res;

    this.tokenService.setAccessToken(accessToken);

    return this.getUserIdFromToken(accessToken);
  }

  public loadUser(id: string): Promise<Employee> {
    return this.http.get<Employee>(`${environment.apiUrl}/employees/${id}`);
  }

  public async loadStoredUser(): Promise<Employee | null> {
    const token = this.tokenService.getAccessToken();

    if (!token) {
      return null;
    }

    const id = this.getUserIdFromToken(token);

    const user = await this.loadUser(id);

    return user;
  }

  public signup(data: SignupDto): Promise<void> {
    return this.http.post(`${environment.apiUrl}/employees`, data);
  }

  public async logout(): Promise<void> {
    this.tokenService.clearTokens();
  }

  public getUserIdFromToken(token: string) {
    return jwtDecode<{ sub: string }>(token)?.sub;
  }
}

export const authService = new AuthService(httpClient, tokenService);
