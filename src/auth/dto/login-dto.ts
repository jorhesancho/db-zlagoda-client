export interface LoginDto {
  password: string;

  employeeId: string;
}
