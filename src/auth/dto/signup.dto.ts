import { Role } from "../../employee/employee.model";

export interface SignupDto {
  id: string;
  surname: string;
  name: string;
  patronymic?: string;
  password: string;
  role: Role;
  salary: number;
  birthDate: string;
  startDate: string;
  phoneNumber: string;
  city: string;
  street: string;
  zipCode: string;
}
