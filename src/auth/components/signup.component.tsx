import React, { FC } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { authService } from "../services/auth.service";
import { useNavigate } from "react-router-dom";
import Navbar from "../../shared/components/nav-bar";

interface SignupFormValues {
  confirmPassword: string;
}

const schema = yup.object().shape({
  username: yup.string().required("Name is required"),
  email: yup
    .string()
    .email("Invalid email address")
    .required("Email is required"),
  password: yup
    .string()
    .min(8, "Password must be at least 8 characters")
    .required("Password is required"),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref("password"), undefined], "Passwords must match")
    .required("Confirm password is required"),
});

const SignupComponent: FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<SignupFormValues>({
    resolver: yupResolver(schema),
  });

  const navigate = useNavigate();

  const onSubmit: SubmitHandler<SignupFormValues> = (data) => {
    // authService.signup(data).then(() => {
    //   navigate("/login");
    // });
  };

  const fields = [
    {
      name: "username",
      type: "text",
      label: "Username",
    },
    {
      name: "email",
      type: "email",
      label: "Email",
    },
    { name: "password", type: "password", label: "Password" },
    {
      name: "confirmPassword",
      type: "password",
      label: "Confirm password",
    },
  ] as const;

  return (
    <>
      <Navbar />
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <h2 className="text-center">Signup</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
              {/* {fields.map((field) => (
                <div className="form-group" key={field.name}>
                  <label htmlFor={field.name}>{field.label}</label>
                  <input
                    type={field.type}
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                  {errors[field.name] && (
                    <div className="invalid-feedback">
                      {errors[field.name]?.message}
                    </div>
                  )}
                </div>
              ))} */}
              <button type="submit" className="btn btn-primary">
                Signup
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default SignupComponent;
