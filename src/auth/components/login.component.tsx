import { FC } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { LoginDto } from "../dto/login-dto";
import { useNavigate } from "react-router-dom";
import { useAuthContext } from "../context/auth-context";
import { fieldConfig, createYupSchema } from "../../shared/utils/field-config";
import Navbar from "../../shared/components/nav-bar";
import { useNotification } from "../../notification/context/NotificationContexProvider";
import { NotificationBar } from "../../notification/components/NotificationBar";

const fields = [
  fieldConfig({
    name: "employeeId",
    type: "text",
    label: "Employee Id",
    validation: yup.string().required("Employee Id is required"),
  }),
  fieldConfig({
    name: "password",
    type: "password",
    label: "Password",
    validation: yup
      .string()
      .min(8, "Password must be at least 8 characters")
      .required("Password is required"),
  }),
] as const;

const schema = createYupSchema(fields);

const LoginComponent: FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<LoginDto>({
    resolver: yupResolver(schema),
  });

  const navigate = useNavigate();
  const { showNotification } = useNotification();

  const { login } = useAuthContext();

  const onSubmit: SubmitHandler<LoginDto> = (data) => {
    login(data)
      .then(() => {
        navigate("/");
      })
      .catch((err) => {
        showNotification({
          title: "Not able to login",
          message: "Please check your credentials",
          status: "error",
        });
      });
  };

  return (
    <>
      <Navbar />
      <NotificationBar />
      <div className="container">
        <div className="row justify-content-center">
          <div className="col-md-6">
            <h2 className="text-center">Login</h2>

            <form onSubmit={handleSubmit(onSubmit)}>
              {fields.map((field) => (
                <div className="form-group" key={field.name}>
                  <label htmlFor={field.name}>{field.label}</label>
                  <input
                    type={field.type}
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                  {errors[field.name] && (
                    <div className="invalid-feedback">
                      {errors[field.name]?.message}
                    </div>
                  )}
                </div>
              ))}

              <button type="submit" className="btn btn-primary">
                Login
              </button>
            </form>
          </div>
        </div>
      </div>
    </>
  );
};

export default LoginComponent;
