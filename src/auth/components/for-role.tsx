import { FC, PropsWithChildren } from "react";
import { useAuthContext } from "../context/auth-context";
import { Role } from "../../employee/employee.model";

export interface ForRolesProps {
  roles: Role[];
}

export const ForRoles: FC<PropsWithChildren<ForRolesProps>> = ({
  roles,
  children,
}) => {
  const { currentUser } = useAuthContext();

  if (!currentUser || !roles.includes(currentUser.role)) {
    return null;
  }

  return <>{children}</>;
};
