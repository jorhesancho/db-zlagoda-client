import { createContext, useContext } from "react";
import { LoginDto } from "../dto/login-dto";
import { Employee } from "../../employee/employee.model";

interface AuthContextData {
  currentUser: Employee | null;
  loading: boolean;
  login: (data: LoginDto) => Promise<void>;
  logout: () => void;
}

export const AuthContext = createContext<AuthContextData>({
  currentUser: null,
  loading: true,
  login: () => Promise.resolve(),
  logout: () => {},
});

export const useAuthContext = () => useContext(AuthContext);
