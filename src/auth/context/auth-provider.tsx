import { FC, PropsWithChildren, useEffect, useState } from "react";
import { AuthContext } from "./auth-context";
import { authService } from "../services/auth.service";
import { LoginDto } from "../dto/login-dto";
import { Employee } from "../../employee/employee.model";

export const AuthProvider: FC<PropsWithChildren> = ({ children }) => {
  const [currentUser, setCurrentUser] = useState<Employee | null>(null);
  const [loading, setLoading] = useState<boolean>(true);

  useEffect(() => {
    authService
      .loadStoredUser()
      .then((user) => {
        setCurrentUser(user);
      })
      .finally(() => {
        setLoading(false);
      });
  }, []);

  const login = (data: LoginDto) => {
    return authService
      .login(data)
      .then((id) =>
        authService.loadUser(id).then((user) => {
          setCurrentUser(user);
        })
      )
      .finally(() => {
        setLoading(false);
      });
  };

  const logout = () => {
    authService.logout();
    setCurrentUser(null);
  };

  return (
    <AuthContext.Provider value={{ currentUser, loading, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
};
