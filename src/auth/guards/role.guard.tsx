import { FC, PropsWithChildren } from "react";
import { Navigate } from "react-router-dom";
import { useAuthContext } from "../context/auth-context";
import { Role } from "../../employee/employee.model";

export interface ForRolesProps {
  roles: Role[];
}

export const RoleGuard: FC<PropsWithChildren<ForRolesProps>> = ({
  children,
  roles,
}) => {
  const { currentUser, loading } = useAuthContext();

  if (loading) {
    return <>Loading...</>;
  }

  if (!currentUser) {
    return <Navigate to="/login" />;
  }

  if (!roles.includes(currentUser.role)) {
    return <Navigate to="/" />;
  }

  return <>{children}</>;
};
