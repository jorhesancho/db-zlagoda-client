import { FC, PropsWithChildren } from "react";
import { Navigate } from "react-router-dom";
import { useAuthContext } from "../context/auth-context";

export const AuthGuard: FC<PropsWithChildren> = ({ children }) => {
  const { currentUser, loading } = useAuthContext();

  if (loading) {
    return <>Loading...</>;
  }

  if (!currentUser) {
    return <Navigate to="/login" />;
  }

  return <>{children}</>;
};
