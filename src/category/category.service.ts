import { environment } from "../environment/environment";
import { HttpClient } from "../shared/http/http-client";
import { CreateCategoryDto } from "./dto/create-category.dto";
import { Category } from "./category.model";

export class CategoryService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(dto: CreateCategoryDto): Promise<void> {
    await this.httpClient.post("/", dto);
  }

  public getAll(): Promise<Category[]> {
    return this.httpClient.get<Category[]>("/");
  }

  public getOneById(id: number): Promise<Category> {
    return this.httpClient.get<Category[]>(`/${id}`).then((res) => res[0]);
  }

  public async update(id: number, dto: CreateCategoryDto): Promise<void> {
    await this.httpClient.patch(`/${id}`, dto);
  }

  public async delete(id: number): Promise<void> {
    await this.httpClient.delete(`/${id}`);
  }
}

export const categoryService = new CategoryService(
  new HttpClient(`${environment.apiUrl}/categories`)
);
