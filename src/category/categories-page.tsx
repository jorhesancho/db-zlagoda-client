import { FC } from "react";
import Layout from "../shared/components/layout";
import { CategoryList } from "./components/category-list";
import { PageTitle } from "../shared/components/page-title";

export const CategoriesPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Categories" />
      <CategoryList />
    </Layout>
  );
};
