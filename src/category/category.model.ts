export class Category {
  number: number;
  name: string;
  productsCount?: number;
}
