import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { Category } from "../category.model";
import { useCategoryActions } from "./use-category-actions";
import { Role } from "../../employee/employee.model";

interface BookActionsProps {
  category: Category;
  reload?: () => any;
}

export const CategoryActions: FC<BookActionsProps> = ({ category, reload }) => {
  const actions = useCategoryActions({
    reload,
  });

  return (
    <ForRoles roles={[Role.Manager]}>
      <div className="d-flex flex-row gap-2">
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => actions.openEditModal(category)}
        >
          Edit
        </button>

        <button
          type="button"
          className="btn btn-danger"
          onClick={() => actions.openDeleteModal(category)}
        >
          Delete
        </button>
      </div>
    </ForRoles>
  );
};
