import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import * as yup from "yup";
import { useModal } from "../../modals/use-modal";
import { pickFrom } from "../../shared/utils/pick";
import { nullable } from "pratica";
import { CreateCategoryDto } from "../dto/create-category.dto";
import { createYupSchema, fieldConfig } from "../../shared/utils/field-config";

const fields = [
  fieldConfig({
    name: "name",
    label: "Name",
    type: "text",
    validation: yup.string().required("Name is required"),
  }),
] as const;

const schema = createYupSchema(fields);

interface CreateCategoryFormProps {
  onSubmit: (data: CreateCategoryDto) => any;
  defaultCategory?: CreateCategoryDto;
}

const mapToCategoryDto = (category: CreateCategoryDto): CreateCategoryDto => {
  return pickFrom(category, ["name"]);
};

const CreateCategoryForm: React.FC<CreateCategoryFormProps> = ({
  onSubmit,
  defaultCategory,
}) => {
  const { closeModal } = useModal();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CreateCategoryDto>({
    defaultValues: nullable(defaultCategory).map(mapToCategoryDto).value(),
    resolver: yupResolver(schema),
  });

  const myOnSubmit = (data: CreateCategoryDto) => {
    closeModal();
    onSubmit(data);
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form
            onSubmit={handleSubmit(myOnSubmit)}
            className="d-flex flex-column justify-content-start gap-3"
          >
            {fields.map((field) => (
              <div
                className="form-group d-flex flex-column align-items-start gap-1"
                key={field.name}
              >
                <label className="fw-bold" htmlFor={field.name}>
                  {field.label}
                </label>

                <input
                  type={field.type}
                  className={`form-control ${
                    errors[field.name] ? "is-invalid" : ""
                  }`}
                  id={field.name}
                  {...register(field.name)}
                />

                {errors[field.name] && (
                  <div className="invalid-feedback">
                    {errors[field.name]?.message}
                  </div>
                )}
              </div>
            ))}

            <button type="submit" className="btn btn-primary mt-3">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateCategoryForm;
