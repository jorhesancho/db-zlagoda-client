import { Modal } from "../../modals/modal";
import { useModal } from "../../modals/use-modal";
import { CreateCategoryDto } from "../dto/create-category.dto";
import { Category } from "../category.model";
import { categoryService } from "../category.service";
import CreateCategoryForm from "./create-category-form";
import DeleteCategoryForm from "./delete-category-form";

export const useCategoryActions = (params: { reload?: () => any }) => {
  const { openModal } = useModal();
  const { reload = () => {} } = params;

  const submitCreateCategory = (data: CreateCategoryDto) => {
    categoryService.create(data).then(() => {
      reload();
    });
  };

  const submitUpdateCategory =
    (categoryId: number) => (data: CreateCategoryDto) => {
      categoryService.update(categoryId, data).then(() => {
        reload();
      });
    };

  const submitDeleteCategory = (categoryId: number) => () => {
    categoryService.delete(categoryId).then(() => {
      reload();
    });
  };

  const openCreateModal = () => {
    openModal(
      <Modal title="Create category">
        <CreateCategoryForm onSubmit={submitCreateCategory} />
      </Modal>
    );
  };

  const openEditModal = (category: Category) => {
    openModal(
      <Modal title="Edit category">
        <CreateCategoryForm
          onSubmit={submitUpdateCategory(category.number)}
          defaultCategory={category}
        />
      </Modal>
    );
  };

  const openDeleteModal = (category: Category) => {
    openModal(
      <Modal title="Delete category">
        <DeleteCategoryForm
          category={category}
          onDelete={submitDeleteCategory(category.number)}
        />
      </Modal>
    );
  };

  return {
    openCreateModal,
    openEditModal,
    openDeleteModal,
  };
};
