import { FC, useEffect, useRef, useState } from "react";
import { CategoryRow } from "./category-row";
import { Category } from "../category.model";
import { categoryService } from "../category.service";
import { CreateCategoryButton } from "./create-category-button";
import { PrintButton } from "../../shared/components/print-btn";

interface CategoryListProps {}

export const CategoryList: FC<CategoryListProps> = () => {
  const [categories, setCategories] = useState<Category[]>([]);

  const loadCategories = () => {
    categoryService.getAll().then(setCategories);
  };

  useEffect(() => {
    loadCategories();
  }, []);

  const reload = () => loadCategories();

  const componentRef = useRef(null);

  return (
    <div className="container mt-5">
      <div className="d-flex gap-2 align-items-center mb-4">
        <PrintButton componentRef={componentRef} />
      </div>

      <table className="table table-striped" ref={componentRef}>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Category Name</th>
            <th scope="col">Product count</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {categories.map((category) => (
            <CategoryRow
              key={category.number}
              category={category}
              reload={reload}
            />
          ))}
        </tbody>
      </table>

      <CreateCategoryButton reload={reload} />
    </div>
  );
};
