import { FC } from "react";
import { Category } from "../category.model";
import { CategoryActions } from "./category-actions";

interface CategoryRowProps {
  category: Category;
  reload?: () => any;
}

export const CategoryRow: FC<CategoryRowProps> = ({ category, reload }) => {
  return (
    <tr className="table-row-link">
      <th scope="row">{category.number}</th>
      <td>{category.name}</td>
      <td>{category.productsCount}</td>
      <td>
        <div className="d-flex flex-row justify-content-end">
          <div className="d-flex flex-row gap-2">
            <CategoryActions category={category} reload={reload} />
          </div>
        </div>
      </td>
    </tr>
  );
};
