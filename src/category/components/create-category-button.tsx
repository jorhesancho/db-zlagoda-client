import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { useCategoryActions } from "./use-category-actions";
import { Role } from "../../employee/employee.model";

interface Props {
  reload: () => any;
}

export const CreateCategoryButton: FC<Props> = ({ reload }) => {
  const { openCreateModal } = useCategoryActions({ reload });

  return (
    <ForRoles roles={[Role.Manager]}>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => openCreateModal()}
      >
        Create Category
      </button>
    </ForRoles>
  );
};
