import { FC } from "react";
import { Employee } from "../employee.model";

export const EmployeeCard: FC<{ employee: Employee }> = ({ employee }) => {
  return (
    <div className="row">
      <div className="col-md-6 offset-md-3">
        <div className="card shadow">
          <div className="card-body">
            <h5 className="card-title">
              <strong>Employee {employee.surname}</strong>
            </h5>

            <div className="dropdown-divider"></div>

            <div className="d-flex flex-column gap-2">
              <p className="card-text m-0">
                <strong>Phone:</strong> {employee.phoneNumber}
              </p>

              <p className="card-text m-0">
                <strong>City:</strong> {employee.city}
              </p>
              <p className="card-text m-0">
                <strong>Street:</strong> {employee.street}
              </p>
              <p className="card-text m-0">
                <strong>Zip code:</strong> {employee.zipCode}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
