import { Link } from "react-router-dom";

const SearchEmployeeButton = () => {
  return (
    <Link to="/employees-search" className="btn btn-primary" role="button">
      Search
    </Link>
  );
};

export default SearchEmployeeButton;
