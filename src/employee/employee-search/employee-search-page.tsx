import { FC } from "react";
import Layout from "../../shared/components/layout";
import EmployeeSearch from "./employee-search";
import { PageTitle } from "../../shared/components/page-title";

export const EmployeeSearchPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Employee Search" />
      <EmployeeSearch />
    </Layout>
  );
};
