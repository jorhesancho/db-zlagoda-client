import { useState } from "react";
import { EmployeeCard } from "./employee-card";
import { employeeService } from "../employee.service";
import { Employee } from "../employee.model";

const EmployeeSearch = () => {
  const [surname, setSurname] = useState("");
  const [employee, setEmployee] = useState<Employee | undefined>(undefined);

  const searchEmployee = () => {
    if (surname) {
      employeeService.getOneBySurname(surname).then((res) => {
        setEmployee(res);
      });
    }
  };

  return (
    <div className="container">
      <div className="row mt-5">
        <div className="col-md-6 offset-md-3">
          <div className="input-group mb-3">
            <input
              type="text"
              className="form-control"
              placeholder="Search employee by surname"
              value={surname}
              onChange={(e) => setSurname(e.target.value)}
            />
            <button
              className="btn btn-outline-primary"
              type="button"
              onClick={searchEmployee}
            >
              Search
            </button>
            <button
              className="btn btn-outline-secondary"
              type="button"
              onClick={() => {
                setSurname("");
                setEmployee(undefined);
              }}
            >
              Clear
            </button>
          </div>
        </div>
      </div>
      {employee && <EmployeeCard employee={employee} />}
    </div>
  );
};

export default EmployeeSearch;
