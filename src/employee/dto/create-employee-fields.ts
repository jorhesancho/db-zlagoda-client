import * as yup from "yup";
import { createYupSchema, fieldConfig } from "../../shared/utils/field-config";

export const createEmployeeFields = [
  fieldConfig({
    name: "id",
    label: "Employee Id",
    type: "text",
    validation: yup.string().required("Id is required"),
  }),

  fieldConfig({
    name: "name",
    label: "Name",
    type: "text",
    validation: yup.string().required("Name is required"),
  }),
  fieldConfig({
    name: "surname",
    label: "Surname",
    type: "text",
    validation: yup.string().required("Surname is required"),
  }),
  fieldConfig({
    name: "patronymic",
    label: "Patronymic",
    type: "text",
    validation: yup.string(),
  }),
  fieldConfig({
    name: "password",
    label: "Password",
    type: "password",
    validation: yup.string().required("Password is required"),
  }),
  fieldConfig({
    name: "role",
    label: "Role",
    type: "select",
    validation: yup.string().required("Role is required"),
  }),
  fieldConfig({
    name: "salary",
    label: "Salary",
    type: "number",
    validation: yup.number().required("Salary is required"),
  }),
  fieldConfig({
    name: "birthDate",
    label: "Birth Date",
    type: "date",
    validation: yup.string().required("Birth Date is required"),
  }),
  fieldConfig({
    name: "startDate",
    label: "Start Date",
    type: "date",
    validation: yup.string().required("Start Date is required"),
  }),
  fieldConfig({
    name: "phoneNumber",
    label: "Phone Number",
    type: "text",
    validation: yup.string().required("Phone Number is required"),
  }),
  fieldConfig({
    name: "city",
    label: "City",
    type: "text",
    validation: yup.string().required("City is required"),
  }),
  fieldConfig({
    name: "street",
    label: "Street",
    type: "text",
    validation: yup.string().required("Street is required"),
  }),
  fieldConfig({
    name: "zipCode",
    label: "ZIP Code",
    type: "text",
    validation: yup.string().required("ZIP Code is required"),
  }),
] as const;

export const createEmployeeSchema = createYupSchema(createEmployeeFields);

export const updateFields = createEmployeeFields.filter(
  (f) => !["password", "id"].includes(f.name)
);

export const updateEmployeeSchema = createYupSchema(updateFields);
