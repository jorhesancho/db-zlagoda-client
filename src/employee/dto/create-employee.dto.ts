import { Role } from "../employee.model";

export class CreateEmployeeDto {
  id: string;
  surname: string;
  name: string;
  patronymic?: string;
  password: string;
  role: Role;
  salary: number;
  birthDate: string;
  startDate: string;
  phoneNumber: string;
  city: string;
  street: string;
  zipCode: string;
}
