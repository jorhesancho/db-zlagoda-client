import { environment } from "../environment/environment";
import { HttpClient } from "../shared/http/http-client";
import { CreateEmployeeDto } from "./dto/create-employee.dto";
import { Employee, Role } from "./employee.model";

export class EmployeeService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(dto: CreateEmployeeDto): Promise<void> {
    await this.httpClient.post("/", dto);
  }

  public getAll(role?: Role): Promise<Employee[]> {
    const params = role ? { role } : {};

    return this.httpClient.get<Employee[]>("/", { params });
  }

  public getOneById(id: string): Promise<Employee> {
    return this.httpClient.get<Employee>(`/${id}`);
  }

  public getOneBySurname(surname: string): Promise<Employee> {
    return this.httpClient.get<Employee>(`/search`, { params: { surname } });
  }

  public async update(id: string, dto: CreateEmployeeDto): Promise<void> {
    await this.httpClient.patch(`/${id}`, dto);
  }

  public async delete(id: string): Promise<void> {
    await this.httpClient.delete(`/${id}`);
  }
}

export const employeeService = new EmployeeService(
  new HttpClient(`${environment.apiUrl}/employees`)
);
