export enum Role {
  Cashier = "cashier",
  Manager = "manager",
}

export class Employee {
  id: string;
  name: string;
  surname: string;
  patronymic: string;
  password: string;
  role: Role;
  salary: number;
  birthDate: string;
  startDate: string;
  phoneNumber: string;
  city: string;
  street: string;
  zipCode: string;
}
