import { FC } from "react";
import Layout from "../shared/components/layout";
import { EmployeeList } from "./components/employee-list";
import { PageTitle } from "../shared/components/page-title";

export const EmployeesPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Employees" />
      <EmployeeList />
    </Layout>
  );
};
