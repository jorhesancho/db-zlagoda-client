import { FC } from "react";
import { Link } from "react-router-dom";
import { Employee } from "../employee.model";

interface ProfileButtonProps {
  currentUser: Employee;
}

export const ProfileButton: FC<ProfileButtonProps> = ({ currentUser }) => {
  const initials = currentUser.name.charAt(0) + currentUser.surname.charAt(0);

  return (
    <Link to="/profile" className="btn btn-link text-decoration-none">
      <div className="d-flex align-items-center">
        <div
          className="rounded-circle bg-primary text-white me-2 d-flex justify-content-center align-items-center"
          style={{ width: "40px", height: "40px" }}
        >
          {initials.toUpperCase()}
        </div>
        <div className="d-flex flex-column align-items-start">
          <div>
            {currentUser.name} {currentUser.surname}
          </div>
          <div className="text-muted" style={{ fontSize: "0.8em" }}>
            {currentUser.role}
          </div>
        </div>
      </div>
    </Link>
  );
};
