import { FC } from "react";
import Layout from "../../shared/components/layout";
import { useAuthContext } from "../../auth/context/auth-context";
import { ProfileCard } from "./profile-card";
import { PageTitle } from "../../shared/components/page-title";

export const ProfilePage: FC = () => {
  const { currentUser } = useAuthContext();

  if (!currentUser) {
    return null;
  }

  return (
    <Layout>
      <PageTitle title="Profile" />
      <ProfileCard employee={currentUser} />
    </Layout>
  );
};
