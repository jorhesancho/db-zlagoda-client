import { FC } from "react";
import { Employee } from "../employee.model";
import { formatDate } from "../../shared/utils/format-date";

export const ProfileCard: FC<{ employee: Employee }> = ({ employee }) => {
  const dateFormat = "DD.MM.YYYY";

  return (
    <div className="row">
      <div className="col-md-6 offset-md-3">
        <div className="card shadow">
          <div className="card-body">
            <h5 className="card-title">
              <strong>
                {employee.name} {employee.surname} {employee.patronymic}
              </strong>
            </h5>

            <div className="dropdown-divider"></div>

            <div className="d-flex flex-column gap-2">
              <p className="card-text m-0">
                <strong>ID:</strong> {employee.id}
              </p>
              <p className="card-text m-0">
                <strong>Role:</strong> {employee.role}
              </p>
              <p className="card-text m-0">
                <strong>Salary:</strong> {employee.salary}
              </p>
              <p className="card-text m-0">
                <strong>Birth Date:</strong>{" "}
                {formatDate(employee.birthDate, dateFormat)}
              </p>
              <p className="card-text m-0">
                <strong>Start Date:</strong>{" "}
                {formatDate(employee.startDate, dateFormat)}
              </p>
              <p className="card-text m-0">
                <strong>Phone:</strong> {employee.phoneNumber}
              </p>
              <p className="card-text m-0">
                <strong>City:</strong> {employee.city}
              </p>
              <p className="card-text m-0">
                <strong>Street:</strong> {employee.street}
              </p>
              <p className="card-text m-0">
                <strong>Zip code:</strong> {employee.zipCode}
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
