import { FC, useCallback, useEffect, useRef, useState } from "react";
import { EmployeeRow } from "./employee-row";
import { Employee, Role } from "../employee.model";
import { employeeService } from "../employee.service";
import { CreateEmployeeButton } from "./create-employee-button";
import SearchEmployeeButton from "../employee-search/search-employee-button";
import { PrintButton } from "../../shared/components/print-btn";

interface EmployeeListProps {}

export const EmployeeList: FC<EmployeeListProps> = () => {
  const [employees, setEmployees] = useState<Employee[]>([]);
  const [filterRole, setFilterRole] = useState<Role | undefined>(undefined);

  const loadEmployees = useCallback(() => {
    employeeService.getAll(filterRole).then(setEmployees);
  }, [filterRole]);

  useEffect(() => {
    loadEmployees();
  }, [filterRole, loadEmployees]);

  const reload = () => loadEmployees();

  const componentRef = useRef(null);

  return (
    <div className="container mt-5">
      <div className="d-flex gap-2 align-items-center mb-4">
        <SearchEmployeeButton />

        <PrintButton componentRef={componentRef} />
      </div>

      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          id="show-cashiers-checkbox"
          checked={filterRole === Role.Cashier}
          onChange={(event) =>
            setFilterRole(event.target.checked ? Role.Cashier : undefined)
          }
        />
        <label className="form-check-label" htmlFor="show-cashiers-checkbox">
          Show cashiers only
        </label>
      </div>

      <table className="table table-striped" ref={componentRef}>
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Surname</th>
            <th scope="col">Patronymic</th>
            <th scope="col">Role</th>
            <th scope="col">Salary</th>
            <th scope="col">Birthdate</th>
            <th scope="col">Start date</th>
            <th scope="col">Phone</th>
            <th scope="col">City</th>
            <th scope="col">Street</th>
            <th scope="col">Zip code</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {employees.map((employee) => (
            <EmployeeRow
              key={employee.id}
              employee={employee}
              reload={reload}
            />
          ))}
        </tbody>
      </table>

      <CreateEmployeeButton reload={reload} />
    </div>
  );
};
