import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { useEmployeeActions } from "./use-employee-actions";
import { Role } from "../employee.model";

interface Props {
  reload: () => any;
}

export const CreateEmployeeButton: FC<Props> = ({ reload }) => {
  const { openCreateModal } = useEmployeeActions({ reload });

  return (
    <ForRoles roles={[Role.Manager]}>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => openCreateModal()}
      >
        Create Employee
      </button>
    </ForRoles>
  );
};
