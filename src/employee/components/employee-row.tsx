import { FC } from "react";
import { Employee } from "../employee.model";
import { EmployeeActions } from "./employee-actions";
import { formatDate } from "../../shared/utils/format-date";

interface EmployeeRowProps {
  employee: Employee;
  reload?: () => any;
}

export const EmployeeRow: FC<EmployeeRowProps> = ({ employee, reload }) => {
  const dateFormat = "DD.MM.YYYY";

  return (
    <tr className="table-row-link">
      <th scope="row">{employee.id}</th>
      <td>{employee.name}</td>
      <td>{employee.surname}</td>
      <td>{employee.patronymic}</td>
      <td>{employee.role}</td>
      <td>{employee.salary}</td>
      <td>{formatDate(employee.birthDate, dateFormat)}</td>
      <td>{formatDate(employee.startDate, dateFormat)}</td>
      <td>{employee.phoneNumber}</td>
      <td>{employee.city}</td>
      <td>{employee.street}</td>
      <td>{employee.zipCode}</td>
      <td>
        <div className="d-flex flex-row justify-content-end">
          <div className="d-flex flex-row gap-2">
            <EmployeeActions employee={employee} reload={reload} />
          </div>
        </div>
      </td>
    </tr>
  );
};
