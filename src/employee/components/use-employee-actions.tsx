import { Modal } from "../../modals/modal";
import { useModal } from "../../modals/use-modal";
import { CreateEmployeeDto } from "../dto/create-employee.dto";
import { Employee } from "../employee.model";
import { employeeService } from "../employee.service";
import CreateEmployeeForm from "./create-employee-form";
import DeleteEmployeeForm from "./delete-employee-form";

export const useEmployeeActions = (params: { reload?: () => any }) => {
  const { openModal } = useModal();
  const { reload = () => {} } = params;

  const submitCreateEmployee = (data: CreateEmployeeDto) => {
    employeeService.create(data).then(() => {
      reload();
    });
  };

  const submitUpdateEmployee =
    (employeeId: string) => (data: CreateEmployeeDto) => {
      employeeService.update(employeeId, data).then(() => {
        reload();
      });
    };

  const submitDeleteEmployee = (employeeId: string) => () => {
    employeeService.delete(employeeId).then(() => {
      reload();
    });
  };

  const openCreateModal = () => {
    openModal(
      <Modal title="Create employee">
        <CreateEmployeeForm onSubmit={submitCreateEmployee} />
      </Modal>
    );
  };

  const openEditModal = (employee: Employee) => {
    openModal(
      <Modal title="Edit employee">
        <CreateEmployeeForm
          onSubmit={submitUpdateEmployee(employee.id)}
          defaultEmployee={employee}
        />
      </Modal>
    );
  };

  const openDeleteModal = (employee: Employee) => {
    openModal(
      <Modal title="Delete employee">
        <DeleteEmployeeForm
          employee={employee}
          onDelete={submitDeleteEmployee(employee.id)}
        />
      </Modal>
    );
  };

  return {
    openCreateModal,
    openEditModal,
    openDeleteModal,
  };
};
