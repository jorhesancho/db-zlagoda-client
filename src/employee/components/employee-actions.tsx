import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { Employee, Role } from "../employee.model";
import { useEmployeeActions } from "./use-employee-actions";

interface BookActionsProps {
  employee: Employee;
  reload?: () => any;
}

export const EmployeeActions: FC<BookActionsProps> = ({ employee, reload }) => {
  const actions = useEmployeeActions({
    reload,
  });

  return (
    <ForRoles roles={[Role.Manager]}>
      <div className="d-flex flex-row gap-2">
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => actions.openEditModal(employee)}
        >
          Edit
        </button>

        <button
          type="button"
          className="btn btn-danger"
          onClick={() => actions.openDeleteModal(employee)}
        >
          Delete
        </button>
      </div>
    </ForRoles>
  );
};
