import React from "react";
import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useModal } from "../../modals/use-modal";
import { pickFrom } from "../../shared/utils/pick";
import { nullable } from "pratica";
import { CreateEmployeeDto } from "../dto/create-employee.dto";
import { Role } from "../employee.model";
import {
  createEmployeeFields,
  createEmployeeSchema,
  updateEmployeeSchema,
  updateFields,
} from "../dto/create-employee-fields";

interface CreateEmployeeFormProps {
  onSubmit: (data: CreateEmployeeDto) => any;
  defaultEmployee?: CreateEmployeeDto;
}

const mapToEmployeeDto = (employee: CreateEmployeeDto): CreateEmployeeDto => {
  return pickFrom(employee, [
    "id",
    "surname",
    "name",
    "patronymic",
    "password",
    "role",
    "salary",
    "birthDate",
    "startDate",
    "phoneNumber",
    "city",
    "street",
    "zipCode",
  ]);
};

const CreateEmployeeForm: React.FC<CreateEmployeeFormProps> = ({
  onSubmit,
  defaultEmployee,
}) => {
  const { closeModal } = useModal();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CreateEmployeeDto>({
    defaultValues: nullable(defaultEmployee).map(mapToEmployeeDto).value(),
    resolver: yupResolver(
      defaultEmployee ? updateEmployeeSchema : createEmployeeSchema
    ),
  });

  const myOnSubmit = (data: CreateEmployeeDto) => {
    closeModal();
    onSubmit({ ...data, patronymic: data.patronymic || undefined });
  };

  const fields = defaultEmployee ? updateFields : createEmployeeFields;

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form onSubmit={handleSubmit(myOnSubmit)}>
            {fields.map((field) => (
              <div className="form-group" key={field.name}>
                <label htmlFor={field.name}>{field.label}</label>
                {field.type === "select" ? (
                  <select
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  >
                    {Object.values(Role).map((role, index) => (
                      <option key={index} value={role}>
                        {role}
                      </option>
                    ))}
                  </select>
                ) : (
                  <input
                    type={field.type}
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                )}
                {errors[field.name] && (
                  <div className="invalid-feedback">
                    {errors[field.name]?.message}
                  </div>
                )}
              </div>
            ))}
            <button type="submit" className="btn btn-primary mt-3">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateEmployeeForm;
