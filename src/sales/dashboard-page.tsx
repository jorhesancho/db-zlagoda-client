import { FC, useEffect, useState } from "react";
import Layout from "../shared/components/layout";
import { PageTitle } from "../shared/components/page-title";
import { GraphData, salesService } from "./sales.service";

export const Dashboard: FC = () => {
  const [stats, setStats] = useState<GraphData[]>([]);
  const [startDate, setStartDate] = useState<string>("2021-01-01");
  const [endDate, setEndDate] = useState<string>("2023-11-01");

  useEffect(() => {
    salesService.statistics(startDate, endDate).then((v) => {
      setStats(v);
    });
  }, [startDate, endDate]);

  return (
    <Layout>
      <PageTitle title="Dashboard" />
      <div className="container">
        <div className="row">
          <div className="col-12">
            <div className="card shadow mb-4">
              <div className="card-header py-3">
                <h1 className="h3 mb-2 text-gray-800">Sales for each day</h1>
              </div>
              <div className="card-body">
                <div className="row">
                  <div className="col-12">
                    <div className="row">
                      <div className="col-6">
                        <div className="form-group">
                          <label htmlFor="startDate">Start Date</label>
                          <input
                            type="date"
                            className="form-control"
                            id="startDate"
                            name="startDate"
                            value={startDate}
                            onChange={(e) => setStartDate(e.target.value)}
                          />
                        </div>
                      </div>
                      <div className="col-6">
                        <div className="form-group">
                          <label htmlFor="endDate">End Date</label>
                          <input
                            type="date"
                            className="form-control"
                            id="endDate"
                            name="endDate"
                            value={endDate}
                            onChange={(e) => setEndDate(e.target.value)}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div className="table-responsive">
                      <table className="table table-bordered">
                        <thead>
                          <tr>
                            <th>Day</th>
                            <th>Quantity</th>
                          </tr>
                        </thead>
                        <tbody>
                          {stats.map((stat: GraphData) => (
                            <tr key={stat.sale_date.toString()}>
                              <td>{stat.sale_date.toString().split("T")[0]}</td>
                              <td>{stat.total_sales}</td>
                            </tr>
                          ))}
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12">
                    <div id="chart"></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
};
