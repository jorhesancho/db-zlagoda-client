import { environment } from "../environment/environment";
import { HttpClient } from "../shared/http/http-client";
import { formatDate } from "../shared/utils/format-date";
import { DateQuery } from "./dto/date.dto";

export interface GraphData {
  sale_date: Date;
  total_sales: number;
}
export class SalesService {
  constructor(private readonly httpClient: HttpClient) {}

  public getTotalRevenue(dateQuery: DateQuery, employeeId?: string) {
    if (employeeId) {
      return this.getTotalRevenueByCashier(employeeId, dateQuery);
    }

    return this.httpClient.get<number>("/revenue", {
      params: this.formatQuery(dateQuery),
    });
  }

  public getTotalRevenueByCashier(employeeId: string, dateQuery: DateQuery) {
    return this.httpClient.get<number>(`/revenue/${employeeId}`, {
      params: this.formatQuery(dateQuery),
    });
  }

  public countSoldProductsByUpc(upc: string, dateQuery: DateQuery) {
    return this.httpClient.get<number>(`/sold-products/${upc}`, {
      params: this.formatQuery(dateQuery),
    });
  }

  private formatQuery(dateQuery: DateQuery) {
    return {
      startDate: formatDate(dateQuery.startDate, "YYYY-MM-DD"),
      endDate: formatDate(dateQuery.endDate, "YYYY-MM-DD"),
    };
  }

  public statistics(startDate: string, endDate: string) {
    return this.httpClient.get<GraphData[]>("dashboard", {
      params: { startDate, endDate },
    });
  }
}

export const salesService = new SalesService(
  new HttpClient(`${environment.apiUrl}/sale`)
);
