import React, { useState } from "react";
import { DateQuery } from "../dto/date.dto";

interface DateFiltersProps {
  onDateChange: (value: DateQuery) => void;
}

export const DateFilters: React.FC<DateFiltersProps> = ({ onDateChange }) => {
  const [startDate, setStartDate] = useState("");
  const [endDate, setEndDate] = useState("");

  const handleDateChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;

    if (name === "startDate") {
      setStartDate(value);
      onDateChange({ startDate: value, endDate });
    } else if (name === "endDate") {
      setEndDate(value);
      onDateChange({ startDate, endDate: value });
    }
  };

  return (
    <div className="d-flex justify-content-center gap-2">
      <div className="input-group mb-3">
        <span className="input-group-text">Start date</span>

        <input
          type="date"
          className="form-control"
          name="startDate"
          value={startDate}
          onChange={handleDateChange}
        />
      </div>

      <div className="input-group mb-3">
        <span className="input-group-text">End date</span>

        <input
          type="date"
          className="form-control"
          name="endDate"
          value={endDate}
          onChange={handleDateChange}
        />
      </div>
    </div>
  );
};
