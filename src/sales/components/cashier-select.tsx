import React, { ChangeEvent, useState } from "react";
import { Employee } from "../../employee/employee.model";

interface SingleEmployeeInputProps {
  employees: Employee[];
  onValueChange: (employee: Employee | undefined) => any;
}

const CashierSelect: React.FC<SingleEmployeeInputProps> = ({
  employees,
  onValueChange,
}) => {
  const [selectedEmployee, setSelectedEmployee] = useState<
    Employee | undefined
  >(undefined);

  const handleEmployeeChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const employee = employees.find((e) => e.id === event.target.value);
    setSelectedEmployee(employee);
    onValueChange(employee);
  };

  return (
    <div className="d-flex">
      <select
        className="form-select"
        value={selectedEmployee?.id || ""}
        onChange={handleEmployeeChange}
      >
        <option value="">All cashiers</option>
        {employees.map((e) => (
          <option key={e.id} value={e.id}>
            {e.name} {e.surname}
          </option>
        ))}
      </select>
    </div>
  );
};

export default CashierSelect;
