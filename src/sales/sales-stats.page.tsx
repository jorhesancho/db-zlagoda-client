import { FC, useEffect, useState } from "react";
import Layout from "../shared/components/layout";
import { PageTitle } from "../shared/components/page-title";
import { DateQuery } from "./dto/date.dto";
import { DateFilters } from "./components/date-filters";
import { Employee, Role } from "../employee/employee.model";
import { employeeService } from "../employee/employee.service";
import CashierSelect from "./components/cashier-select";
import { salesService } from "./sales.service";

export const SaleStatsPage: FC = () => {
  const [dateQuery, setDateQuery] = useState<DateQuery>({
    startDate: "",
    endDate: "",
  });
  const [employee, setEmployee] = useState<Employee | undefined>();
  const [employees, setEmployees] = useState<Employee[]>([]);
  const [revenue, setRevenue] = useState<number | undefined>(undefined);

  useEffect(() => {
    employeeService.getAll(Role.Cashier).then(setEmployees);
  }, []);

  const isValidQuery = (dates: DateQuery) => {
    return !!dates.startDate && !!dates.endDate;
  };

  useEffect(() => {
    if (!isValidQuery(dateQuery)) {
      return;
    }
    salesService.getTotalRevenue(dateQuery, employee?.id).then(setRevenue);
  }, [dateQuery, employee]);

  return (
    <Layout>
      <PageTitle title="Sales Stats" />

      <div className="container">
        <div>
          <DateFilters onDateChange={setDateQuery} />
          <CashierSelect onValueChange={setEmployee} employees={employees} />
        </div>

        {revenue !== undefined && isValidQuery(dateQuery) && (
          <div className="card shadow p-4 my-4">
            <strong>Total revenue:</strong> {revenue}
          </div>
        )}
      </div>
    </Layout>
  );
};
