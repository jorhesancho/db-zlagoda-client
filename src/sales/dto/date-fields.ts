import * as yup from "yup";
import { createYupSchema, fieldConfig } from "../../shared/utils/field-config";

export const dateFields = [
  fieldConfig({
    name: "startDate",
    label: "Start Date",
    type: "date",
    validation: yup.string().required("Start Date is required"),
  }),
  fieldConfig({
    name: "endDate",
    label: "End Date",
    type: "date",
    validation: yup.string().required("End Date is required"),
  }),
] as const;

export const dateSchema = createYupSchema(dateFields);
