import { formatDate } from "../../shared/utils/format-date";

export class DateQuery {
  startDate: string;
  endDate: string;
}

export const formatDateQuery = (dateQuery: DateQuery) => {
  return {
    startDate: formatDate(dateQuery.startDate, "YYYY-MM-DD"),
    endDate: formatDate(dateQuery.endDate, "YYYY-MM-DD"),
  };
};
