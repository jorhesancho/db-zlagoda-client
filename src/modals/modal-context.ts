import { createContext } from "react";

interface ModalContextType {
  showModal: boolean;
  modalContent: React.ReactNode | null;
  closeModal: () => void;
  openModal: (content: React.ReactNode) => void;
}

const ModalContext = createContext<ModalContextType>({
  showModal: false,
  modalContent: null,
  closeModal: () => {},
  openModal: () => {},
});

export default ModalContext;
