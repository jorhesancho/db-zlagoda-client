// ModalProvider.tsx
import React, { FC, PropsWithChildren, useState } from "react";
import ModalContext from "./modal-context";

type Content = React.ReactNode | null;

const ModalProvider: FC<PropsWithChildren> = ({ children }) => {
  const [showModal, setShowModal] = useState(false);
  const [modalContent, setModalContent] = useState<Content>(null);

  const closeModal = () => {
    setShowModal(false);
    setModalContent(null);
  };

  const openModal = (content: React.ReactNode) => {
    setModalContent(content);
    setShowModal(true);
  };

  return (
    <ModalContext.Provider
      value={{ showModal, modalContent, openModal, closeModal }}
    >
      {children}
    </ModalContext.Provider>
  );
};

export default ModalProvider;
