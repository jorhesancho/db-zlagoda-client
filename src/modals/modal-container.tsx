import { FC } from "react";
import { useModal } from "./use-modal";

export const ModalContainer: FC = () => {
  const { showModal, closeModal, modalContent } = useModal();

  return (
    <>
      {showModal && <div className="modal-backdrop fade show"></div>}
      <div
        className={`modal fade${showModal ? " show d-block" : ""}`}
        tabIndex={-1}
        role="dialog"
        aria-hidden="true"
        onClick={closeModal}
      >
        <div
          className="modal-dialog"
          role="document"
          onClick={(e) => e.stopPropagation()}
        >
          {modalContent}
        </div>
      </div>
    </>
  );
};
