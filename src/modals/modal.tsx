import { FC, PropsWithChildren } from "react";
import { useModal } from "./use-modal";

export interface ModalProps {
  title: string;
  showFooter?: boolean;
}

export const Modal: FC<PropsWithChildren<ModalProps>> = ({
  title,
  showFooter = false,
  children,
}) => {
  const { closeModal } = useModal();

  return (
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="dynamicModalLabel">
          {title}
        </h5>
        <button
          type="button"
          className="btn-close"
          aria-label="Close"
          onClick={closeModal}
        ></button>
      </div>

      <div className="modal-body">{children}</div>

      {showFooter && (
        <div className="modal-footer">
          <button
            type="button"
            className="btn btn-secondary"
            onClick={closeModal}
          >
            Close
          </button>
        </div>
      )}
    </div>
  );
};
