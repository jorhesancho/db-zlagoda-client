import { CustomerCard } from "../customer-card.model";
import {
  CreateCustomerCardFormValues,
  CreateCustomerCardDto,
} from "./create-customer-card.dto";

export const toDto = ({
  cardNumber,
  name,
  percent,
  phoneNumber,
  surname,
  city,
  patronymic,
  street,
  zipCode,
}: CreateCustomerCardFormValues): CreateCustomerCardDto => {
  return {
    cardNumber,
    phoneNumber,
    percent,
    address: {
      city: city || undefined,
      street: street || undefined,
      zipCode: zipCode || undefined,
    },
    customer: {
      name,
      surname,
      patronymic: patronymic || undefined,
    },
  };
};

export const toFormValues = ({
  cardNumber,
  percent,
  phoneNumber,
  address: { city, street, zipCode },
  customer: { name, surname, patronymic },
}: CustomerCard): CreateCustomerCardFormValues => {
  return {
    cardNumber,
    phoneNumber,
    percent,
    city,
    street,
    zipCode,
    name,
    surname,
    patronymic,
  };
};
