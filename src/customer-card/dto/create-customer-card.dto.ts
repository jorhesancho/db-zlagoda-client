export class CardAddressDto {
  public city?: string;
  public street?: string;
  public zipCode?: string;
}

export class CardCustomerDto {
  surname: string;
  name: string;
  patronymic?: string;
}

export class CreateCustomerCardDto {
  public cardNumber: string;
  public customer: CardCustomerDto;
  public phoneNumber: string;
  public address: CardAddressDto;
  public percent: number;
}

export type CreateCustomerCardFormValues = Omit<
  CreateCustomerCardDto,
  "address" | "customer"
> &
  CardAddressDto &
  CardCustomerDto;
