import * as yup from "yup";
import { createYupSchema, fieldConfig } from "../../shared/utils/field-config";

export const createCustomerCardFields = [
  fieldConfig({
    name: "cardNumber",
    label: "Card Number",
    type: "text",
    validation: yup.string().required("Card Number is required"),
  }),
  fieldConfig({
    name: "surname",
    label: "Surname",
    type: "text",
    validation: yup.string().required("Surname is required"),
  }),
  fieldConfig({
    name: "name",
    label: "Name",
    type: "text",
    validation: yup.string().required("Name is required"),
  }),
  fieldConfig({
    name: "patronymic",
    label: "Patronymic",
    type: "text",
    validation: yup.string().notRequired(),
  }),
  fieldConfig({
    name: "percent",
    label: "Percent",
    type: "number",
    validation: yup.number().required("Percent is required"),
  }),
  fieldConfig({
    name: "phoneNumber",
    label: "Phone Number",
    type: "text",
    validation: yup.string().required("Phone Number is required"),
  }),
  fieldConfig({
    name: "city",
    label: "City",
    type: "text",
    validation: yup.string().notRequired(),
  }),
  fieldConfig({
    name: "street",
    label: "Street",
    type: "text",
    validation: yup.string().notRequired(),
  }),
  fieldConfig({
    name: "zipCode",
    label: "ZIP Code",
    type: "text",
    validation: yup.string().notRequired(),
  }),
] as const;

export const createCustomerCardSchema = createYupSchema(
  createCustomerCardFields
);
