import { environment } from "../environment/environment";
import { HttpClient } from "../shared/http/http-client";
import { CreateCustomerCardDto } from "./dto/create-customer-card.dto";
import { CustomerCard } from "./customer-card.model";

export class CustomerCardService {
  constructor(private readonly httpClient: HttpClient) {}

  public async create(dto: CreateCustomerCardDto): Promise<void> {
    await this.httpClient.post("/", dto);
  }

  public getAll(): Promise<CustomerCard[]> {
    return this.httpClient.get<CustomerCard[]>("/");
  }

  public getOneById(id: string): Promise<CustomerCard> {
    return this.httpClient.get<CustomerCard>(`/${id}`);
  }

  public async update(id: string, dto: CreateCustomerCardDto): Promise<void> {
    await this.httpClient.patch(`/${id}`, dto);
  }

  public async delete(id: string): Promise<void> {
    await this.httpClient.delete(`/${id}`);
  }
}

export const customerCardService = new CustomerCardService(
  new HttpClient(`${environment.apiUrl}/customer-cards`)
);
