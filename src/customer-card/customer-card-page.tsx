import { FC } from "react";
import Layout from "../shared/components/layout";
import { CustomerCardList } from "./components/customer-card-list";
import { PageTitle } from "../shared/components/page-title";

export const CustomerCardsPage: FC = () => {
  return (
    <Layout>
      <PageTitle title="Customer Cards" />
      <CustomerCardList />
    </Layout>
  );
};
