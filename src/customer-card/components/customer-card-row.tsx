import { FC } from "react";
import { CustomerCard } from "../customer-card.model";
import { CustomerCardActions } from "./customer-card-actions";

interface CustomerCardRowProps {
  customerCard: CustomerCard;
  reload?: () => any;
}

export const CustomerCardRow: FC<CustomerCardRowProps> = ({
  customerCard,
  reload,
}) => {
  return (
    <tr className="table-row-link">
      <th scope="row">{customerCard.cardNumber}</th>
      <td>{customerCard.customer.surname}</td>
      <td>{customerCard.customer.name}</td>
      <td>{customerCard.customer.patronymic}</td>
      <td>{customerCard.percent}</td>
      <td>{customerCard.phoneNumber}</td>
      <td>{customerCard.address.city}</td>
      <td>{customerCard.address.street}</td>
      <td>{customerCard.address.zipCode}</td>
      <td>
        <div className="d-flex flex-row justify-content-end">
          <div className="d-flex flex-row gap-2">
            <CustomerCardActions customerCard={customerCard} reload={reload} />
          </div>
        </div>
      </td>
    </tr>
  );
};
