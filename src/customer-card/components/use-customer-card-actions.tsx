import { Modal } from "../../modals/modal";
import { useModal } from "../../modals/use-modal";
import { CreateCustomerCardDto } from "../dto/create-customer-card.dto";
import { CustomerCard } from "../customer-card.model";
import { customerCardService } from "../customer-card.service";
import CreateCustomerCardForm from "./create-card-form";
import DeleteCustomerCardForm from "./delete-card-form";

export const useCustomerCardActions = (params: { reload?: () => any }) => {
  const { openModal } = useModal();
  const { reload = () => {} } = params;

  const submitCreateCustomerCard = (data: CreateCustomerCardDto) => {
    customerCardService.create(data).then(() => {
      reload();
    });
  };

  const submitUpdateCustomerCard =
    (customerCardId: string) => (data: CreateCustomerCardDto) => {
      customerCardService.update(customerCardId, data).then(() => {
        reload();
      });
    };

  const submitDeleteCustomerCard = (customerCardId: string) => () => {
    customerCardService.delete(customerCardId).then(() => {
      reload();
    });
  };

  const openCreateModal = () => {
    openModal(
      <Modal title="Create customerCard">
        <CreateCustomerCardForm onSubmit={submitCreateCustomerCard} />
      </Modal>
    );
  };

  const openEditModal = (customerCard: CustomerCard) => {
    openModal(
      <Modal title="Edit customerCard">
        <CreateCustomerCardForm
          onSubmit={submitUpdateCustomerCard(customerCard.cardNumber)}
          defaultCustomerCard={customerCard}
        />
      </Modal>
    );
  };

  const openDeleteModal = (customerCard: CustomerCard) => {
    openModal(
      <Modal title="Delete customerCard">
        <DeleteCustomerCardForm
          customerCard={customerCard}
          onDelete={submitDeleteCustomerCard(customerCard.cardNumber)}
        />
      </Modal>
    );
  };

  return {
    openCreateModal,
    openEditModal,
    openDeleteModal,
  };
};
