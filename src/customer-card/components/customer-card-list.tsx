import { FC, useEffect, useRef, useState } from "react";
import { CustomerCardRow } from "./customer-card-row";
import { CustomerCard } from "../customer-card.model";
import { customerCardService } from "../customer-card.service";
import { CreateCustomerCardButton } from "./create-card-button";
import { PrintButton } from "../../shared/components/print-btn";

interface CustomerCardListProps {}

export const CustomerCardList: FC<CustomerCardListProps> = () => {
  const [customerCards, setCustomerCards] = useState<CustomerCard[]>([]);

  const loadCustomerCards = () => {
    customerCardService.getAll().then(setCustomerCards);
  };

  useEffect(() => {
    loadCustomerCards();
  }, []);

  const reload = () => loadCustomerCards();

  const componentRef = useRef(null);

  return (
    <div className="container mt-5">
      <div className="d-flex gap-2 align-items-center mb-4">
        <PrintButton componentRef={componentRef} />
      </div>

      <table className="table table-striped" ref={componentRef}>
        <thead>
          <tr>
            <th scope="col">Card number</th>
            <th scope="col">Surname</th>
            <th scope="col">Name</th>
            <th scope="col">Patronymic</th>
            <th scope="col">Percent</th>
            <th scope="col">Phone</th>
            <th scope="col">City</th>
            <th scope="col">Street</th>
            <th scope="col">Zip code</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
          {customerCards.map((customerCard) => (
            <CustomerCardRow
              key={customerCard.cardNumber}
              customerCard={customerCard}
              reload={reload}
            />
          ))}
        </tbody>
      </table>

      <CreateCustomerCardButton reload={reload} />
    </div>
  );
};
