import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { CustomerCard } from "../customer-card.model";
import { useCustomerCardActions } from "./use-customer-card-actions";
import { Role } from "../../employee/employee.model";

interface BookActionsProps {
  customerCard: CustomerCard;
  reload?: () => any;
}

export const CustomerCardActions: FC<BookActionsProps> = ({
  customerCard,
  reload,
}) => {
  const actions = useCustomerCardActions({
    reload,
  });

  return (
    <div className="d-flex flex-row gap-2">
      <ForRoles roles={[Role.Manager, Role.Cashier]}>
        <button
          type="button"
          className="btn btn-secondary"
          onClick={() => actions.openEditModal(customerCard)}
        >
          Edit
        </button>
      </ForRoles>

      <ForRoles roles={[Role.Manager]}>
        <button
          type="button"
          className="btn btn-danger"
          onClick={() => actions.openDeleteModal(customerCard)}
        >
          Delete
        </button>
      </ForRoles>
    </div>
  );
};
