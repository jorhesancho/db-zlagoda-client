import { FC } from "react";
import { ForRoles } from "../../auth/components/for-role";
import { useCustomerCardActions } from "./use-customer-card-actions";
import { Role } from "../../employee/employee.model";

interface Props {
  reload: () => any;
}

export const CreateCustomerCardButton: FC<Props> = ({ reload }) => {
  const { openCreateModal } = useCustomerCardActions({ reload });

  return (
    <ForRoles roles={[Role.Manager]}>
      <button
        type="button"
        className="btn btn-primary"
        onClick={() => openCreateModal()}
      >
        Add a customer card
      </button>
    </ForRoles>
  );
};
