import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useModal } from "../../modals/use-modal";
import { nullable } from "pratica";
import {
  CreateCustomerCardDto,
  CreateCustomerCardFormValues,
} from "../dto/create-customer-card.dto";
import {
  createCustomerCardFields,
  createCustomerCardSchema,
} from "../dto/create-customer-card-fields";
import { CustomerCard } from "../customer-card.model";
import { toDto, toFormValues } from "../dto/mappers";

interface CreateCustomerCardFormProps {
  onSubmit: (data: CreateCustomerCardDto) => any;
  defaultCustomerCard?: CustomerCard;
}

const CreateCustomerCardForm: React.FC<CreateCustomerCardFormProps> = ({
  onSubmit,
  defaultCustomerCard,
}) => {
  const { closeModal } = useModal();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CreateCustomerCardFormValues>({
    defaultValues: nullable(defaultCustomerCard).map(toFormValues).value(),
    resolver: yupResolver(createCustomerCardSchema),
  });

  const myOnSubmit = (data: CreateCustomerCardFormValues) => {
    closeModal();
    onSubmit(toDto(data));
  };

  const fields = createCustomerCardFields;

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form
            onSubmit={handleSubmit(myOnSubmit)}
            className="d-flex flex-column justify-content-start gap-3"
          >
            {fields.map((field) => (
              <div
                className="form-group d-flex flex-column align-items-start gap-1"
                key={field.name}
              >
                <label className="fw-bold" htmlFor={field.name}>
                  {field.label}
                </label>
                {
                  <input
                    type={field.type}
                    className={`form-control ${
                      errors[field.name] ? "is-invalid" : ""
                    }`}
                    id={field.name}
                    {...register(field.name)}
                  />
                }
                {errors[field.name] && (
                  <div className="invalid-feedback">
                    {errors[field.name]?.message}
                  </div>
                )}
              </div>
            ))}

            <button type="submit" className="btn btn-primary mt-3">
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateCustomerCardForm;
