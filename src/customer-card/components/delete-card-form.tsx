import React, { FC } from "react";
import { useModal } from "../../modals/use-modal";
import { CustomerCard } from "../customer-card.model";

interface DeleteCustomerCardFormProps {
  onDelete: () => void;
  customerCard: CustomerCard;
}

const DeleteCustomerCardForm: FC<DeleteCustomerCardFormProps> = ({
  onDelete,
  customerCard,
}) => {
  const { closeModal } = useModal();

  const myOnDelete = () => {
    closeModal();
    onDelete();
  };

  return (
    <div className="d-flex flex-column gap-3 align-items-start text-start">
      <p className="fs-5 m-0">
        Are you sure you want to delete the customer card "
        {customerCard.cardNumber}"?
      </p>

      <div className="d-flex flex-row align-items-start gap-3">
        <button type="button" className="btn btn-danger" onClick={myOnDelete}>
          Delete
        </button>

        <button
          type="button"
          className="btn btn-secondary"
          onClick={closeModal}
        >
          Cancel
        </button>
      </div>
    </div>
  );
};

export default DeleteCustomerCardForm;
