export class CustomerCard {
  public cardNumber: string;
  public customer: {
    surname: string;
    name: string;
    patronymic?: string;
  };
  public address: {
    city?: string;
    street?: string;
    zipCode?: string;
  };
  public phoneNumber: string;
  public percent: number;
}
