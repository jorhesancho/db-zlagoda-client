import React, { useState, useEffect } from "react";
import SingleSaleInput from "./single-sale-input";
import { StoreProduct } from "../store-product/store-product.model";
import { storeProductService } from "../store-product/store-product.service";

export type SaleFormValue = {
  storeProduct?: StoreProduct | undefined;
  amount: number;
};

export type ValidSaleFormValue = Required<SaleFormValue>;

interface SalesListProps {
  salesInputs: SaleFormValue[];
  setSalesInputs: (v: SaleFormValue[]) => any;
}

const SaleInputList: React.FC<SalesListProps> = ({
  salesInputs,
  setSalesInputs,
}) => {
  const [storeProducts, setStoreProducts] = useState<StoreProduct[]>([]);

  useEffect(() => {
    storeProductService.getAll().then(setStoreProducts);
  }, []);

  const addSaleInput = () => {
    setSalesInputs([...salesInputs, { amount: 0 }]);
  };

  const editSaleInput = (index: number, value: SaleFormValue) => {
    const inputs = [...salesInputs];
    inputs[index] = value;
    setSalesInputs([...inputs]);
  };

  const removeSaleInput = (index: number) => {
    setSalesInputs(salesInputs.filter((_, i) => i !== index));
  };

  return (
    <div>
      <div className="d-flex flex-column gap-2">
        {salesInputs.map((_, index) => (
          <SingleSaleInput
            key={index}
            storeProducts={storeProducts}
            onRemove={() => removeSaleInput(index)}
            onValueChange={(v) => editSaleInput(index, v)}
          />
        ))}
      </div>

      <button
        type="button"
        className="btn btn-primary mt-3"
        onClick={addSaleInput}
      >
        Add Sale
      </button>
    </div>
  );
};

export default SaleInputList;
