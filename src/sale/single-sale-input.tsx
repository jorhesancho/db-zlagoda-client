import React, { useState, ChangeEvent } from "react";
import { StoreProduct } from "../store-product/store-product.model";
import { SaleFormValue } from "./sales-list-input";

interface SingleSaleInputProps {
  storeProducts: StoreProduct[];
  onRemove: () => void;
  onValueChange: (value: SaleFormValue) => any;
}

const SingleSaleInput: React.FC<SingleSaleInputProps> = ({
  storeProducts,
  onRemove,
  onValueChange,
}) => {
  const [selectedProduct, setSelectedProduct] = useState<
    StoreProduct | undefined
  >(undefined);
  const [amount, setAmount] = useState<number>(0);

  const handleAmountChange = (event: ChangeEvent<HTMLInputElement>) => {
    setAmount(Number(event.target.value));
    onValueChange({
      amount: Number(event.target.value),
      storeProduct: selectedProduct,
    });
  };

  const handleProductChange = (event: ChangeEvent<HTMLSelectElement>) => {
    const pr = storeProducts.find((sp) => sp.upc === event.target.value);
    setSelectedProduct(pr);
    onValueChange({ amount, storeProduct: pr });
  };

  return (
    <div className="d-flex">
      <select
        className="form-select"
        value={selectedProduct?.upc || ""}
        onChange={handleProductChange}
      >
        <option value="">Select a product</option>

        {storeProducts.map((sp) => (
          <option key={sp.upc} value={sp.upc}>
            {sp.product?.name}
          </option>
        ))}
      </select>

      <input
        className="form-control ms-2"
        type="number"
        min="0"
        value={amount}
        onChange={handleAmountChange}
      />

      <button type="button" className="btn btn-danger ms-2" onClick={onRemove}>
        Remove
      </button>
    </div>
  );
};

export default SingleSaleInput;
