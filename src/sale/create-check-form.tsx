import { useForm } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { useModal } from "../modals/use-modal";
import { CreateCheckDto } from "../check/dto/create-check.dto";
import { createYupSchema } from "../shared/utils/field-config";
import { createCheckFields } from "../check/dto/create-check-fields";
import { useAuthContext } from "../auth/context/auth-context";
import { formatDate } from "../shared/utils/format-date";
import SaleInputList, {
  SaleFormValue,
  ValidSaleFormValue,
} from "./sales-list-input";
import { useState } from "react";
import { CreateSaleDto } from "../check/dto/create-sale.dto";

const schema = createYupSchema(createCheckFields);

interface CreateCheckFormProps {
  onSubmit: (data: CreateCheckDto) => any;
  defaultCheck?: CreateCheckDto;
}

export type CreateFormValues = Omit<
  CreateCheckDto,
  "employeeId" | "sales" | "printDate" | "sumTotal" | "valueAddedTax"
>;

const CreateCheckForm: React.FC<CreateCheckFormProps> = ({
  onSubmit,
  defaultCheck,
}) => {
  const { closeModal } = useModal();

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<CreateFormValues>({
    defaultValues: defaultCheck,
    resolver: yupResolver(schema),
  });

  const { currentUser } = useAuthContext();

  const myOnSubmit = (data: CreateFormValues) => {
    closeModal();

    if (!isValidSalesList(salesInputs)) {
      return;
    }

    const sumTotal = salesInputs.reduce(
      (sum, sale) => sum + sale.amount * sale.storeProduct.sellingPrice,
      0
    );

    const sales: CreateSaleDto[] = salesInputs.map((s) => ({
      upc: s.storeProduct.upc,
      amount: s.amount,
      sellingPrice: s.storeProduct.sellingPrice,
    }));

    const vat = sumTotal * 0.2;

    onSubmit({
      ...data,
      employeeId: currentUser?.id as string,
      printDate: formatDate(new Date()),
      sumTotal,
      valueAddedTax: vat,
      sales,
    });
  };

  const [salesInputs, setSalesInputs] = useState<SaleFormValue[]>([]);

  const isValidSalesList = (
    list: SaleFormValue[]
  ): list is ValidSaleFormValue[] => {
    return salesInputs.every(
      (sale) =>
        sale.storeProduct !== undefined &&
        Number.isSafeInteger(sale.amount) &&
        sale.amount > 0
    );
  };

  const isValidCurrentSalesList = () => {
    return (
      salesInputs.length > 0 &&
      salesInputs.every(
        (sale) =>
          sale.storeProduct !== undefined &&
          Number.isSafeInteger(sale.amount) &&
          sale.amount > 0
      )
    );
  };

  return (
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-6">
          <form
            onSubmit={handleSubmit(myOnSubmit)}
            className="d-flex flex-column justify-content-start gap-3"
          >
            {createCheckFields.map((field) => (
              <div
                className="form-group d-flex flex-column align-items-start gap-1"
                key={field.name}
              >
                <label className="fw-bold" htmlFor={field.name}>
                  {field.label}
                </label>
                <input
                  type={field.type}
                  className={`form-control ${
                    errors[field.name] ? "is-invalid" : ""
                  }`}
                  id={field.name}
                  {...register(field.name)}
                />
                {errors[field.name] && (
                  <div className="invalid-feedback">
                    {errors[field.name]?.message}
                  </div>
                )}
              </div>
            ))}

            <SaleInputList
              salesInputs={salesInputs}
              setSalesInputs={setSalesInputs}
            />

            <button
              disabled={!isValidCurrentSalesList()}
              type="submit"
              className="btn btn-primary mt-3"
            >
              Submit
            </button>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreateCheckForm;
