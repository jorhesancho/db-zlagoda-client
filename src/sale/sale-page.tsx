import { FC } from "react";
import Layout from "../shared/components/layout";
import CreateCheckForm from "./create-check-form";
import { checkService } from "../check/check.service";
import { CreateCheckDto } from "../check/dto/create-check.dto";
import { useNavigate } from "react-router-dom";
import { PageTitle } from "../shared/components/page-title";

export const SalePage: FC = () => {
  const navigate = useNavigate();

  const handleSubmit = (value: CreateCheckDto) => {
    checkService.create(value).then(() => {
      navigate("/checks");
    });
  };

  return (
    <Layout>
      <PageTitle title="Sale" />

      <div className="card shadow p-5 m-3">
        <CreateCheckForm onSubmit={handleSubmit} />
      </div>
    </Layout>
  );
};
